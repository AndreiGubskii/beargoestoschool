﻿using UnityEngine;
using System.Collections;
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
using UnityEngine.iOS;
#endif

public class Clothing : MonoBehaviour
{
    public delegate void ClothingEvents(Clothing clothing);

    public event ClothingEvents OnPointerDown;
    public event ClothingEvents OnPointerUp;
    private Mover _mover;
    private bool scale;
    private Vector3 targetScale;
    private float speedScale = 10f;

    [System.Serializable]
    public class ClothingTransform
    {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
        public int layer;
    }

    [SerializeField] private ObjectsTypes.Clothing clothingType;
    [SerializeField] private ClothingTransform[] startTransformArray;
    [SerializeField] private ClothingTransform[] iPadStartTransformArray;
    [SerializeField] private ClothingTransform[] iPhoneXStartTransformArray;
    private ClothingTransform startTransform;
    private Canvas canvas;
    private void Update()
    {
        if (this.scale)
        {
            if (this.transform.localScale != this.targetScale)
            {
                this.transform.localScale = Vector3.MoveTowards(
                    this.transform.localScale,
                    this.targetScale,
                    this.speedScale * Time.deltaTime
                    );
            }
            else
            {
                this.scale = false;
                if (this.transform.localScale == Vector3.zero)
                {
                    this.gameObject.SetActive(false);
                }
            }
        }
    }

    private ClothingTransform[] GetStartTransformArray()
    {
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
            if (Device.generation.ToString().IndexOf("iPad") > -1) {
                return this.iPadStartTransformArray;
            }
            else if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXR ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXS ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXSMax) {
                return this.iPhoneXStartTransformArray;
            }
            else {
                return this.startTransformArray;
            }
#endif
#if (UNITY_ANDROID || UNITY_WSA) && !UNITY_IOS
        return this.startTransformArray;
#endif

    }

    private void OnEnable()
    {
        int i = Random.Range(0, this.GetStartTransformArray().Length);
        this.startTransform = this.GetStartTransformArray()[i];

        this.transform.localPosition = this.startTransform.position;
        this.transform.localRotation = Quaternion.Euler(this.startTransform.rotation);
        this.transform.localScale = this.startTransform.scale;
        GetCanvas().sortingOrder = this.startTransform.layer;
        this.scale = false;
    }

    public ClothingTransform GetStarTransform()
    {
        return this.startTransform;
    }

    public ObjectsTypes.Clothing GetClothingType()
    {
        return this.clothingType;
    }

    public Transform GetTransform()
    {
        return this.transform;
    }

    public Canvas GetCanvas()
    {
        if (this.canvas == null)
        {
            this.canvas = GetComponent<Canvas>();
        }
        return this.canvas;
    }

    private void OnMouseDown()
    {
        if (this.OnPointerDown != null)
        {
            this.OnPointerDown(this);
        }
    }

    private void OnMouseUp()
    {
        if (this.OnPointerUp != null)
        {
            this.OnPointerUp(this);
        }
    }

    public Mover GetMover()
    {
        if (this._mover == null)
        {
            this._mover = GetComponent<Mover>();
        }
        return this._mover;
    }

    public void ScaleTo(Vector3 target, float speed = 10f)
    {
        this.targetScale = target;
        this.scale = true;
        this.speedScale = speed;
    }
}
