﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
using UnityEngine.iOS;
#endif
public class ClothingSearch : MonoBehaviour {
    public static ClothingSearch instance;
    private ObjectsTypes.Clothing clothing;
    
    private List<ObjectsTypes.Clothing> clothingTypesList;
    private Clothing[] clothingArray;
    private bool end;
    private bool choiced;
    [SerializeField] private Vector3 counterPosition;
    [SerializeField] private Vector3 iPadCounterPosition;
    [SerializeField] private Vector3 androidCounterPosition;
    [SerializeField] private UIBearAnimationController bearAnimation;
    [SerializeField] private Scaler textPanelScaler;
    [SerializeField] private Text text;
    [SerializeField] private CounterController counter;
	void Start () {
	    instance = this;
        this.clothingArray = FindObjectsOfType<Clothing>();
        foreach (Clothing obj in clothingArray) {
	        obj.OnPointerUp += this.OnPointerUpListener;
	        obj.OnPointerDown += this.OnPointerDownListener;
	    }
        this.Restart();
    }
    
    // Вызывается в Invoke
    private void ChoiceOfCloth() {
        if (clothingTypesList.Count > 0) {
            int i = Random.Range(0, clothingTypesList.Count);
            clothing = clothingTypesList[i];
            this.LoadText(clothing);
            clothingTypesList.Remove(clothingTypesList[i]);
            this.choiced = true;
        }
        else {
            this.end = true;
            this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/WellDone").text;
            this.bearAnimation.PlaySmileOnce();
            AudioClip voice = SoundsManager.instance.GetVoiceByName("18");
            SoundsManager.instance.PlayVoice(voice);
            if (MainManager.instance != null) {
                MainManager.instance.ShowEndPanel(voice.length, 0.3f, SoundsManager.instance.GetVoiceByName("18_1"));
            }
        }
    }

    public void CompareClothing(ObjectsTypes.Clothing clothing,Clothing clothingObj) {
        if(!this.choiced) return;
        this.choiced = false;
        if (this.GetClothing() == clothing) {
            StopAllCoroutines(); // Останавливаем LoadTextIEnumerator 
            clothingObj.GetCanvas().sortingOrder = 5;
            PlayVoiceIsСorrectly(clothing);
            Invoke("ChoiceOfCloth",2.85f);
            clothingObj.GetMover().MoveTo(this.counterPosition,5300f);
            clothingObj.ScaleTo(Vector3.zero,5f);
            this.bearAnimation.PlaySmileAnimaton();
        }
        else {
            this.choiced = true;
            StopAllCoroutines(); // Останавливаем LoadTextIEnumerator 
            AudioClip voice = SoundsManager.instance.GetVoiceByName("12");
            SoundsManager.instance.PlayVoice(voice);
            text.text = LocalizationManager.Instance.LoadResource("Scene_03/TryAgain").text;
            this.bearAnimation.PlaySadAnimation();
            LoadText(this.GetClothing(), voice.length);
        }
    }

    public ObjectsTypes.Clothing GetClothing() {
        return this.clothing;
    }

    private void LoadText(ObjectsTypes.Clothing clothing,float delay = 0f) {
        StartCoroutine(this.LoadTextIEnumerator(clothing, delay));
    }

    private IEnumerator LoadTextIEnumerator(ObjectsTypes.Clothing clothing,float delay = 0f) {
        yield return new WaitForSeconds(delay);
        switch (clothing) {
            case ObjectsTypes.Clothing.CAP:
                this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Cap").text;
                SoundsManager.instance.PlayVoice("11");
                break;
            case ObjectsTypes.Clothing.BACKPACK:
                this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Backpack").text;
                SoundsManager.instance.PlayVoice("16");
                break;
            case ObjectsTypes.Clothing.PANTS:
                this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Pants").text;
                SoundsManager.instance.PlayVoice("17");
                break;
            case ObjectsTypes.Clothing.SHOE:
                this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Shoe").text;
                SoundsManager.instance.PlayVoice("14");
                break;
            case ObjectsTypes.Clothing.SINGLET:
                this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Singlet").text;
                SoundsManager.instance.PlayVoice("15");
                break;
            default:
                break;
        }
    }

    private void OnPointerDownListener(Clothing clothing) {
        if(this.end) return;
        clothing.GetTransform().localScale *= 1.3f;
        clothing.GetCanvas().sortingOrder = 5;
    }

    private void OnPointerUpListener(Clothing clothing) {
        if(this.end) return;
        clothing.GetTransform().localScale /= 1.3f;
        clothing.GetCanvas().sortingOrder = clothing.GetStarTransform().layer;

        if (SoundsManager.instance.GetVoiceAudioSource().clip.name == "10") return;
        this.CompareClothing(clothing.GetClothingType(), clothing);
    }

    public void Restart() {
        SoundsManager.instance.PlayBackSound("search_sound");
        textPanelScaler.ScaleTo(new Vector3(0,1,1),100f);
        AudioClip voice = SoundsManager.instance.GetVoiceByName("10");
        SoundsManager.instance.PlayVoice(voice);
        textPanelScaler.ScaleTo(voice.length,new Vector3(1, 1, 1), 5f);
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
        if (Device.generation.ToString().IndexOf("iPad") > -1) {
            this.counterPosition = iPadCounterPosition;
        }else{
            this.counterPosition = androidCounterPosition;
        }
#endif
#if (UNITY_ANDROID || UNITY_WSA) && !UNITY_IOS
        this.counterPosition = androidCounterPosition;
#endif
        this.end = false;
        this.choiced = false;
        clothingTypesList = new List<ObjectsTypes.Clothing>();
        clothingTypesList.Add(ObjectsTypes.Clothing.BACKPACK);
        clothingTypesList.Add(ObjectsTypes.Clothing.CAP);
        clothingTypesList.Add(ObjectsTypes.Clothing.PANTS);
        clothingTypesList.Add(ObjectsTypes.Clothing.SHOE);
        clothingTypesList.Add(ObjectsTypes.Clothing.SINGLET);
        this.bearAnimation.Reset();
        this.counter.ResetCounter();
        this.counter.SetMaxCount(this.clothingArray.Length);
        this.counter.SetText(this.counter.GetCount(), this.counter.GetMaxCount());
        foreach (Clothing cl in clothingArray) {
            cl.gameObject.SetActive(true);
        }
        Invoke("ChoiceOfCloth",voice.length);
    }

    private void PlayVoiceIsСorrectly(ObjectsTypes.Clothing clothing) {
        this.text.text = LocalizationManager.Instance.LoadResource("Scene_03/Correctly").text;
        switch (clothing) {
            case ObjectsTypes.Clothing.CAP:
                SoundsManager.instance.PlayVoice("13_4");
                break;
            case ObjectsTypes.Clothing.BACKPACK:
                SoundsManager.instance.PlayVoice("13_1");
                break;
            case ObjectsTypes.Clothing.PANTS:
                SoundsManager.instance.PlayVoice("13_5");
                break;
            case ObjectsTypes.Clothing.SHOE:
                SoundsManager.instance.PlayVoice("13_3");
                break;
            case ObjectsTypes.Clothing.SINGLET:
                SoundsManager.instance.PlayVoice("13_2");
                break;
            default:
                break;
        }
    }
}
