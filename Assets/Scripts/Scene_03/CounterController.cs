﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CounterController : MonoBehaviour {
    [SerializeField] private Text text;
    private int i;
    private int maxCount;
    private void OnEnable() {
        this.i = 0;
    }

    private void OnTriggerEnter(Collider col) {
        col.gameObject.SetActive(false);
        this.Count();
    }

    private IEnumerator ScaleIEnumerator() {
        this.transform.localScale *= 1.2f;
        yield return new WaitForSeconds(0.15f);
        this.transform.localScale /= 1.2f;
    }

    public String GetText() {
        return this.text.text;
    }

    public void SetText(int a,int b) {
        this.text.text = a+"/"+b;
    }

    public void Count() {
        this.i++;
        this.SetText(this.GetCount(), this.GetMaxCount());
        StartCoroutine(this.ScaleIEnumerator());
    }


    public int GetCount() {
        return this.i;
    }

    public int GetMaxCount() {
        return this.maxCount;
    }

    public void SetMaxCount(int count) {
        this.maxCount = count;
    }

    public void ResetCounter() {
        this.i = 0;
    }
}
