﻿using UnityEngine;
using System.Collections;

public class Page_02 : Page {
    [SerializeField] private TouchHandler alarm;
    [SerializeField] private Head02AnimationController _headAnimator;
    private bool activeAlarm;
    private new void OnEnable(){
        base.OnEnable();
        alarm.PointerUpEvent += ClickOnAlarmListener;
        this.activeAlarm = false;
        AudioClip voice = SoundsManager.instance.GetVoiceByName("2");
        SoundsManager.instance.PlayVoice(voice);
        StartCoroutine(PlayAlarmAnimation(voice.length + 1f));
        SoundsManager.instance.Play(alarm.gameObject.GetComponent<AudioSource>(),
            SoundsManager.instance.GetAudioFromResources("Sounds/BackSounds/OtherSounds/alarm_tick"),
            true);
    }

    private new void OnDisable(){
        base.OnDisable();
    }

    private void ClickOnAlarmListener(GameObject al) {
        if(!this.activeAlarm) return;
        al.GetComponent<Animator>().SetTrigger("stopAlarm");
        SoundsManager.instance.Play(alarm.gameObject.GetComponent<AudioSource>(),
            SoundsManager.instance.GetAudioFromResources("Sounds/BackSounds/OtherSounds/alarm_tick"),
            true);
        this._headAnimator.PlayOpenEyes();
        PageChanger.instance.NextPage(3f);
    }

    private IEnumerator PlayAlarmAnimation(float delay) {
        yield return new WaitForSeconds(delay);
        this.activeAlarm = true;
        alarm.gameObject.GetComponent<Animator>().SetTrigger("playAlarm");
        SoundsManager.instance.Play(alarm.gameObject.GetComponent<AudioSource>(),
            SoundsManager.instance.GetAudioFromResources("Sounds/BackSounds/OtherSounds/alarm"),
            true);
    }
}
