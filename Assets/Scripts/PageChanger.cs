﻿using UnityEngine;
using System.Collections;


public class PageChanger : MonoBehaviour {
    public static PageChanger instance;
    public delegate void TaleEvents();

    public event TaleEvents ChangePage; 

    [SerializeField] private GameObject[] pages;
    private bool nextPageClicked;
    private bool previousPageClicked;

    private void Awake() {
        instance = this;
        this.nextPageClicked = false;
        this.previousPageClicked = false;
    }

    private void Start() {
        SoundsManager.instance.PlayBackSound("start_sound");
    }

    public void NextPage(float delay = 0f) {
        if (this.nextPageClicked) return;
        StartCoroutine(this.NextPageEnumerator(delay));
    }

    private IEnumerator NextPageEnumerator(float delay) {
        this.nextPageClicked = true;
        yield return new WaitForSeconds(delay);
        for (int i = 0; i < pages.Length - 1; i++) {
            if ((i+1) == 1 && SoundsManager.instance.GetBackMusicAudioSource().clip.name != "scene_1_sound") {
                SoundsManager.instance.PlayBackSound("scene_1_sound");
            }
            if (pages[i].activeSelf) {
                pages[i].SetActive(false);
                pages[i + 1].SetActive(true);
                break;
            }
        }
        if (this.ChangePage != null) {
            this.ChangePage();
        }
        this.nextPageClicked = false;
    }

    public void PreviousPage(float delay = 0f) {
        if (this.previousPageClicked) return;
        StartCoroutine(this.PreviousPageEnumerator(delay));
    }

    private IEnumerator PreviousPageEnumerator(float delay) {
        this.previousPageClicked = true;
        yield return new WaitForSeconds(delay);
        for (int i = 0; i <= pages.Length - 1; i++) {
            if ((i - 1) == 0) {
                SoundsManager.instance.PlayBackSound("start_sound");
            }
            if (pages[i].activeSelf && i != 0) {
                pages[i].SetActive(false);
                pages[i - 1].SetActive(true);
                break;
            }
        }
        if (this.ChangePage != null){
            this.ChangePage();
        }
        this.previousPageClicked = false;
    }

    public void Activate() {
        if (!this.pages[0].activeSelf) {
            this.pages[0].SetActive(true);
        }
    }

    public void Deactivate() {
        foreach (var page in pages) {
            if (page.activeSelf) {
                page.SetActive(false);
            }
        }
    }
}
