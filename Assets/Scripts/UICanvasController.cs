﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UICanvasController : MonoBehaviour {
    [System.Serializable]
    public class CanvasButtons{
        public TouchHandler nextBtn;
        public TouchHandler previousBtn;
        public TouchHandler loadSceneBtn;
        public TouchHandler miniGamesBtn;
        public TouchHandler miniGamesCloseBtn;
    }

    public static UICanvasController instance;
    [SerializeField] private TouchHandler[] miniGamesBtns;
    [SerializeField] private CanvasButtons buttons;
    [SerializeField] private GameObject textPanel;
    [SerializeField] private GameObject[] miniGamesPanel;
    private void Awake() {
        instance = this;
    }

    private void Start() {
        foreach (GameObject o in miniGamesPanel) {
            if (o.transform.localScale != Vector3.zero){
                o.transform.localScale = Vector3.zero;
            }
        }

        foreach (TouchHandler btn in miniGamesBtns) {
            btn.PointerDownEvent += MiniGameBtnDownListener;
            btn.PointerUpEvent += MiniGameBtnUpListener;
        }

        buttons.nextBtn.PointerUpEvent += NextBtnPointerUpListener;
        buttons.previousBtn.PointerUpEvent += PreviousBtnPointerUpListener;
        buttons.loadSceneBtn.PointerUpEvent += LoadSceneBtnPointerUpListener;
        buttons.nextBtn.PointerDownEvent += NextBtnPointerDownListener;
        buttons.previousBtn.PointerDownEvent += PreviousBtnPointerDownListener;
        buttons.loadSceneBtn.PointerDownEvent += LoadSceneBtnPointerDownListener;

        buttons.miniGamesBtn.PointerDownEvent += MiniGamesDownListener;
        buttons.miniGamesBtn.PointerUpEvent += MiniGamesUpListener;

        buttons.miniGamesCloseBtn.PointerDownEvent += MiniGamesDownListener;
        buttons.miniGamesCloseBtn.PointerUpEvent += MiniGamesUpListener;
    }

    private void NextBtnPointerDownListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        btn.transform.localScale *= 1.1f;
    }

    private void PreviousBtnPointerDownListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        btn.transform.localScale *= 1.1f;
    }

    private void LoadSceneBtnPointerDownListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        btn.transform.localScale *= 1.1f;
    }

    private void NextBtnPointerUpListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        btn.transform.localScale /= 1.1f;
        PageChanger.instance.NextPage(0.3f);
    }

    private void PreviousBtnPointerUpListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        btn.transform.localScale /= 1.1f;
        PageChanger.instance.PreviousPage(0.3f);
    }

    private void LoadSceneBtnPointerUpListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        btn.transform.localScale /= 1.1f;
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1) {
            Loading.level = 1 + SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene("Loading");
        }
    }

    private void MiniGameBtnDownListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        btn.transform.localScale *= 1.1f;
    }

    private void MiniGameBtnUpListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        btn.transform.localScale /= 1.1f;

        if (MainManager.instance != null) {
            MainManager.instance.LoadMiniGame(btn.GetComponent<BtnController>());
        }
    }

    private void MiniGamesDownListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        btn.transform.localScale *= 1.1f;
    }

    private void MiniGamesUpListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        btn.transform.localScale /= 1.1f;
        this.ShowOrHideMiniGamesPanel();
        
    }


    public void ShowPreviousBtn() {
        if (!this.buttons.previousBtn.gameObject.activeSelf) {
            this.buttons.previousBtn.gameObject.SetActive(true);
        }
    }

    public void HidePreviousBtn() {
        if (this.buttons.previousBtn.gameObject.activeSelf) {
            this.buttons.previousBtn.gameObject.SetActive(false);
        }
    }

    public void ShowMiniGamesBtn() {
        if (!this.buttons.miniGamesBtn.gameObject.activeSelf) {
            this.buttons.miniGamesBtn.gameObject.SetActive(true);
        }
    }

    public void HideMiniGamesBtn() {
        if (this.buttons.miniGamesBtn.gameObject.activeSelf) {
            this.buttons.miniGamesBtn.gameObject.SetActive(false);
        }
    }

    public void ShowNextBtn() {
        if (!this.buttons.nextBtn.gameObject.activeSelf) {
            this.buttons.nextBtn.gameObject.SetActive(true);
        }
    }

    public void HideNextBtn() {
        if (this.buttons.nextBtn.gameObject.activeSelf) {
            this.buttons.nextBtn.gameObject.SetActive(false);
        }
    }

    public void HideLoadSceneBtn() {
        if (this.buttons.loadSceneBtn.gameObject.activeSelf) {
            this.buttons.loadSceneBtn.gameObject.SetActive(false);
        }
    }

    public void ShowLoadSceneBtn() {
        if (!this.buttons.loadSceneBtn.gameObject.activeSelf) {
            this.buttons.loadSceneBtn.gameObject.SetActive(true);
        }
    }

    public void HideTextPanel() {
        if (this.textPanel.activeSelf) {
            this.textPanel.SetActive(false);
        }
    }

    public void ShowTextPanel() {
        if (!this.textPanel.activeSelf) {
            this.textPanel.SetActive(true);
        }
    }

    public CanvasButtons GetButtons() {
        return this.buttons;
    }

    private void ShowOrHideMiniGamesPanel() {
        if (buttons.miniGamesBtn.gameObject.activeSelf) {
            HideMiniGamesBtn();
        }
        else {
            ShowMiniGamesBtn();
        }
        foreach (GameObject o in miniGamesPanel) {
            if (o.transform.localScale == Vector3.one){
                o.GetComponent<Scaler>().ScaleTo(Vector3.zero);
            }
            else {
                o.GetComponent<Scaler>().ScaleTo(Vector3.one);
            }
        }

    }
}
