﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class BathroomManager : MonoBehaviour {
    public static BathroomManager instance;

    [SerializeField] private BrushController brush;
    [SerializeField] private SpongeController sponge;
    [SerializeField] private ShowerheadController showerhead;
    [SerializeField] private NapkinController napkin;
    [SerializeField] private Vector3 defaultPosition;
    [SerializeField] private Vector3 startPosition;
    [SerializeField] private float napkinTimer;
    [SerializeField] private WaterDropCreator waterDropCreator;
    [SerializeField] private BathroomBearAnimationController bearAnimation;
    private List<GameObject> bubblsList;
    private float speed = 750f;
    private bool brushed;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        this.Restart();

    }

#region TeethBrush
     private void ActivateBrush(float delay = 0f) {
         StartCoroutine(this.ActivateBrushIEnumerator(delay));
     }

    private IEnumerator ActivateBrushIEnumerator(float delay = 0f) {
        yield return new WaitForSeconds(delay);
        this.bubblsList = new List<GameObject>();
        if (this.brush != null) {
            brush.transform.localPosition = this.defaultPosition;
            brush.gameObject.SetActive(true);
            brush.BrushedTeeth += BrushedTeethListener;
            brush.GetMover().MoveTo(startPosition,this.speed);
            brush.gameObject.GetComponent<GizmosController>().IsDragable = true;
            this.brushed = false;
            this.bearAnimation.PlayOpenMouth();
        }
    }

    private void PreDeactivateBrush() {
        brush.BrushedTeeth -= BrushedTeethListener;
        brush.GetMover().OnBackToStartPosition += DeactivateBrush;
        brush.gameObject.GetComponent<GizmosController>().IsDragable = false;
        this.brush.GetMover().MoveTo(this.defaultPosition, this.speed);
        this.TeethBubbleDestroy(1f);
    }

    private void DeactivateBrush() {
        brush.GetMover().OnBackToStartPosition -= DeactivateBrush;
        this.brush.gameObject.SetActive(false);
        AudioClip voice = SoundsManager.instance.GetVoiceByName("5");
        SoundsManager.instance.PlayVoice(voice);
        this.ActivateSponge(voice.length);
    }

    private void BrushedTeethListener() {
        this.PreDeactivateBrush();
    }

    public void TeethBubbleDestroy(float delay = 0f) {
        StartCoroutine(this.DestroyBubbleIEnumerator(delay));
    }

    private IEnumerator DestroyBubbleIEnumerator(float delay = 0f) {
        this.brushed = true;
        yield return new WaitForSeconds(delay);
        int count = this.GetBubbleList().Count;
        for (int i = 0; i < count;i++) {
            Destroy(this.GetBubbleList()[i]);
            yield return new WaitForSeconds(0.01f);
        }
        //this.GetBubbleList().Clear();
    }

    public bool IsTeethBrushed() {
        return this.brushed;
    }

    public void SetTeethBrushed(bool brushed) {
        this.brushed = brushed;
    }

    #endregion

#region Sponge
    private void ActivateSponge(float delay = 0f) {
         StartCoroutine(this.ActivateSpongeIEnumerator(delay));
     }

    private IEnumerator ActivateSpongeIEnumerator(float delay = 0f) {
        yield return new WaitForSeconds(delay);
        SoundsManager.instance.PlayVoice("6");
        this.bubblsList = new List<GameObject>();
        if (this.sponge != null) {
            sponge.transform.localPosition = this.defaultPosition;
            sponge.gameObject.SetActive(true);
            sponge.BrushedFace += BrushedFaceListener;
            sponge.GetMover().MoveTo(startPosition,this.speed);
            sponge.gameObject.GetComponent<GizmosController>().IsDragable = true;
            this.brushed = false;
            this.bearAnimation.PlayCloseMouth();
            yield return new WaitForSeconds(0.5f);
            this.bearAnimation.PlayCloseEyes();
        }
    }

    private void PreDeactivateSponge() {
        sponge.BrushedFace -= BrushedFaceListener;
        sponge.GetMover().OnBackToStartPosition += DeactivateSponge;
        sponge.gameObject.GetComponent<GizmosController>().IsDragable = false;
        this.sponge.GetMover().MoveTo(this.defaultPosition, this.speed);
    }

    private void DeactivateSponge() {
        sponge.GetMover().OnBackToStartPosition -= DeactivateSponge;
        this.sponge.gameObject.SetActive(false);
        this.ActivateShowerhead(1.5f);
    }

    private void BrushedFaceListener() {
        this.PreDeactivateSponge();
    }
    #endregion

#region Showerhead
    private void ActivateShowerhead(float delay = 0f) {
         StartCoroutine(this.ActivateShowerheadIEnumerator(delay));
     }

    private IEnumerator ActivateShowerheadIEnumerator(float delay = 0f) {
        yield return new WaitForSeconds(delay);
        SoundsManager.instance.PlayVoice("7");
        if (this.showerhead != null) {
            this.showerhead.transform.localPosition = this.defaultPosition;
            this.showerhead.gameObject.SetActive(true);
            this.showerhead.ShowerheadWashed += this.ShowerheadWashedListener;
            this.showerhead.GetMover().MoveTo(startPosition,this.speed);
            this.showerhead.gameObject.GetComponent<GizmosController>().IsDragable = true;
            this.waterDropCreator.SetActivate(true);
            this.brushed = false;
        }
    }

    private void PreDeactivateShowerhead() {
        this.showerhead.ShowerheadWashed -= this.ShowerheadWashedListener;
        this.showerhead.GetMover().OnBackToStartPosition += this.DeactivateShowerhead;
        this.showerhead.gameObject.GetComponent<GizmosController>().IsDragable = false;
        this.showerhead.GetMover().MoveTo(this.defaultPosition, this.speed);
        
    }

    private void DeactivateShowerhead() {
        this.showerhead.GetMover().OnBackToStartPosition -= this.DeactivateShowerhead;
        this.showerhead.gameObject.SetActive(false);
        this.ActivateNapkin(1.5f);

    }

    private void ShowerheadWashedListener() {
        this.SetTeethBrushed(true);
        this.PreDeactivateShowerhead();
    }
    #endregion

#region Napkin
    private void ActivateNapkin(float delay = 0f) {
         StartCoroutine(this.ActivateNapkinIEnumerator(delay));
     }

    private IEnumerator ActivateNapkinIEnumerator(float delay = 0f) {
        yield return new WaitForSeconds(delay);
        SoundsManager.instance.PlayVoice("8");
        if (this.napkin != null) {
            this.napkin.transform.localPosition = this.defaultPosition;
            this.napkin.gameObject.SetActive(true);
            this.napkin.Wiped += this.WipedListener;
            this.napkin.GetMover().MoveTo(startPosition,this.speed);
            this.napkin.gameObject.GetComponent<GizmosController>().IsDragable = true;
            this.brushed = false;
        }
    }

    private void PreDeactivateNapkin() {
        this.showerhead.ShowerheadWashed -= this.ShowerheadWashedListener;
        this.napkin.GetMover().OnBackToStartPosition += this.DeactivateNapkin;
        this.napkin.gameObject.GetComponent<GizmosController>().IsDragable = false;
        this.napkin.GetMover().MoveTo(this.defaultPosition, this.speed);
        this.bearAnimation.PlayOpenEyes();
    }

    private void DeactivateNapkin() {
        this.napkin.GetMover().OnBackToStartPosition -= this.DeactivateShowerhead;
        this.napkin.gameObject.SetActive(false);
        AudioClip voice = SoundsManager.instance.GetVoiceByName("9");
        SoundsManager.instance.PlayVoice(voice);

        if (MainManager.instance != null) {
            MainManager.instance.ShowEndPanel(voice.length,0.3f,SoundsManager.instance.GetVoiceByName("9_1"));
        }
        
    }

    private void WipedListener() {
        this.waterDropCreator.SetActivate(false);
        this.SetTeethBrushed(true);
        this.PreDeactivateNapkin();
    }

    public float GetNapkinTimer() {
        return this.napkinTimer;
    }

#endregion

    public void RemoveBubbleList(GameObject bubble) {
        if (this.bubblsList.Count != 0) {
            this.bubblsList.Remove(bubble);
        }
    }

    public void AddBubbleList(GameObject bubble) {
        this.bubblsList.Add(bubble);
    }

    public List<GameObject> GetBubbleList() {
        return this.bubblsList;
    }

    public Vector3 GetStartPosition() {
        return this.startPosition;
    }

    public float GetSpeed() {
        return this.speed;
    }

    public void Restart() {
        SoundsManager.instance.PlayVoice("4");
        SoundsManager.instance.PlayBackSound("bathroom_sound");
        this.ActivateBrush(3.2f);
    }

}
