﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class WaterDrop : MonoBehaviour {
    private SpriteRenderer spriteRenderer;
    private float i = 1;
    private float speed = 1f;
    private void Start() {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        this.transform.Translate(Vector3.down * this.speed*Time.deltaTime);
        this.spriteRenderer.color = new Color(this.spriteRenderer.color.r,
            this.spriteRenderer.color.g, this.spriteRenderer.color.b, i-=0.4f*Time.deltaTime);//Color.Lerp(, Color.clear, 5 * Time.deltaTime);
        if (this.spriteRenderer.color.a < 0.1f) {
            Destroy(gameObject);
        }
    }
}
