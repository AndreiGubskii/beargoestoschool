﻿using System;
using UnityEngine;
using System.Collections;

public class SpongeController : MonoBehaviour {
    public delegate void SpongeEvents();

    public event SpongeEvents Activate;
    public event SpongeEvents Deactivate;
    public event SpongeEvents BrushingFace;
    public event SpongeEvents BrushedFace;

    [SerializeField] private GameObject bubble;
    [SerializeField] private Transform parent;
    [SerializeField] private float timer;
    [SerializeField] private int countBubbleMax;

    private Mover mover;
    private Vector3 lastPosition;
    private float startTime = 0f;

    private void OnEnable() {
        if (this.Activate != null) {
            this.Activate();
        }
        if (this.mover == null) {
            this.mover = GetComponent<Mover>();
        }
    }

    private void OnDisable() {
        if (this.Deactivate != null) {
            this.Deactivate();
        }
    }

    private void OnTriggerStay(Collider col) {
        if (col.gameObject.name == "head" &&
            this.transform.position != this.lastPosition) {
            if (BathroomManager.instance.GetBubbleList().Count < this.countBubbleMax) {
                if ((startTime + timer) > Time.time) return;
                this.startTime = Time.time;
                GameObject obj = (GameObject) Instantiate(bubble);
                obj.transform.SetParent(parent);
                obj.transform.localScale = new Vector3(50f, 50f, 50f);
                obj.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y,
                    130f);
                obj.name = "Bubble";
                this.lastPosition = this.transform.position;
                BathroomManager.instance.AddBubbleList(obj);
                if (this.BrushingFace != null) {
                    this.BrushingFace();
                }
                if (!SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying && this.isToachDown) {
                    SoundsManager.instance.PlayOtherSound("waterbuble", true);
                }
            }
            else {
                if (!BathroomManager.instance.IsTeethBrushed()) {
                    if (this.BrushedFace != null) {
                        this.BrushedFace();
                    }
                }
            }
        }
    }

    private bool isToachDown;

    private void OnMouseDown() {
        this.isToachDown = true;
    }

    private void OnMouseUp() {
        this.isToachDown = false;
        if(BathroomManager.instance.IsTeethBrushed()) return;
        if (SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
            SoundsManager.instance.GetOtherSoundsAudioSource().Stop();
        }
        if (BathroomManager.instance.GetBubbleList().Count > this.countBubbleMax/2) {
            if (this.BrushedFace != null) {
                this.BrushedFace();
            }
        }
        else {
            this.mover.MoveTo(BathroomManager.instance.GetStartPosition(), BathroomManager.instance.GetSpeed());
        }
    }

    public Mover GetMover() {
        return this.mover;
    }
}
