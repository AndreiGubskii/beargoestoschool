﻿using UnityEngine;
using System.Collections;

public class WaterDropCreator : MonoBehaviour {
    [SerializeField] private Vector3[] spawns;
    [SerializeField] private GameObject waterDrop;
    [SerializeField] private float timer;
    private float time;
    private bool activate;

    public void SetActivate(bool activate) {
        this.activate = activate;
    }

    private void Update () {
	    if (activate) {
	        if ((this.time + this.timer) < Time.time) {
	            GameObject obj = (GameObject)Instantiate(this.waterDrop);
                obj.transform.SetParent(this.transform);
                obj.transform.localScale = new Vector3(100f,100f,100f);
	            obj.transform.localPosition = this.spawns[Random.Range(0, spawns.Length)];
	            this.time = Time.time;
	        }
	    }
	}
}
