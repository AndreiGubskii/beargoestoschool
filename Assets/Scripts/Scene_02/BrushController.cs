﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BrushController : MonoBehaviour {
    public delegate void BrushEvents();

    public event BrushEvents Activate;
    public event BrushEvents Deactivate;
    public event BrushEvents BrushingTeeth;
    public event BrushEvents BrushedTeeth;

    [SerializeField] private GameObject bubble;
    [SerializeField] private Transform parent;
    [SerializeField] private float timer;
    [SerializeField] private int countBubbleMax;

    private Vector3 lastPosition;
    private float startTime = 0f;

    private Mover mover;
    private bool isToachDown;

    private void OnEnable() {
        if (this.Activate != null) {
            this.Activate();
        }
        if (this.mover == null) {
            this.mover = GetComponent<Mover>();
        }
    }

    private void OnDisable() {
        if (this.Deactivate != null) {
            this.Deactivate();
        }
    }

    private void FixedUpdate() {
        if(Vector3.Distance(this.transform.localPosition,new Vector3(-220,-300,0)) > 330)
        if (SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
            SoundsManager.instance.GetOtherSoundsAudioSource().Stop();
        }
    }

    private void OnTriggerStay(Collider col) {
        if (col.gameObject.name == "mouth" && this.transform.position != this.lastPosition) {
            if (BathroomManager.instance.GetBubbleList().Count < this.countBubbleMax) {
                if ((startTime + timer) > Time.time) return;
                this.startTime = Time.time;
                GameObject obj = (GameObject)Instantiate(bubble);
                obj.transform.SetParent(parent);
                obj.transform.localScale = new Vector3(20f, 20f, 20f);
                obj.transform.localPosition = new Vector3(this.transform.localPosition.x - 150, this.transform.localPosition.y, 0f);
                this.lastPosition = this.transform.position;
                BathroomManager.instance.AddBubbleList(obj);
                if (this.BrushingTeeth != null) {
                    this.BrushingTeeth();
                }
                if (!SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying && this.isToachDown) {
                    SoundsManager.instance.PlayOtherSound("teeth", true);
                }
            }
            else {
                if (!BathroomManager.instance.IsTeethBrushed()) {
                    if (this.BrushedTeeth != null) {
                        this.BrushedTeeth();
                    }
                }
            }
        }
    }

    private void OnMouseDown() {
        this.isToachDown = true;
    }

    private void OnMouseUp() {
        this.isToachDown = false;
        if (SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
            SoundsManager.instance.GetOtherSoundsAudioSource().Stop();
        }
        if(BathroomManager.instance.IsTeethBrushed()) return;
        if (BathroomManager.instance.GetBubbleList().Count > this.countBubbleMax/2) {
            if (this.BrushedTeeth != null) {
                this.BrushedTeeth();
            }
        }
        else {
            this.mover.MoveTo(BathroomManager.instance.GetStartPosition(), BathroomManager.instance.GetSpeed());
        }
    }

    public Mover GetMover() {
        return this.mover;
    }

}
