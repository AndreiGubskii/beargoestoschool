﻿using UnityEngine;
using System.Collections;

public class BathroomBearAnimationController : MonoBehaviour {
    private Animator _animator;

    private void Awake() {
        this._animator = GetComponent<Animator>();
    }

    public void PlayOpenMouth() {
        this._animator.SetTrigger("openMouth");
    }

    public void PlayCloseMouth() {
        this._animator.SetTrigger("closeMouth");
    }

    public void PlayOpenEyes() {
        this._animator.SetTrigger("openEyes");
    }

    public void PlayCloseEyes() {
        this._animator.SetTrigger("closeEyes");
    }
}
