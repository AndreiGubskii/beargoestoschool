﻿using UnityEngine;
using System.Collections;

public class ShowerheadController : MonoBehaviour {
    public delegate void ShowerheadEvents();

    public event ShowerheadEvents Activate;
    public event ShowerheadEvents Deactivate;
    public event ShowerheadEvents ShowerheadWash;
    public event ShowerheadEvents ShowerheadWashed;
    
    [SerializeField] private Transform[] waterSpawns;
    [SerializeField] private GameObject waterDrop;

    private Mover mover;

    private void OnEnable() {
        if (this.Activate != null) {
            this.Activate();
        }
        if (this.mover == null) {
            this.mover = GetComponent<Mover>();
        }
    }

    private void OnDisable() {
        if (this.Deactivate != null) {
            this.Deactivate();
        }
    }
	
	void Update () {
	    if (!BathroomManager.instance.IsTeethBrushed()) {
	        if (Input.GetMouseButton(0)) {
	            this.Water();
	            if (this.ShowerheadWash != null) {
	                this.ShowerheadWash();
	            }
                if (!SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
                    SoundsManager.instance.PlayOtherSound("shower",true);
                }
	        }

	        if (BathroomManager.instance.GetBubbleList().Count == 0) {
	            if (this.ShowerheadWashed != null) {
	                this.ShowerheadWashed();
	            }
                if (SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
                    SoundsManager.instance.GetOtherSoundsAudioSource().Stop();
                }
	        }
	    }
	}

    private float waterTime = 0;
    private void Water() {
        if ((this.waterTime + 0.1f) > Time.time) return;
        for (int i = 0; i < 7; i++) {
            int spawnID = Random.Range(0, waterSpawns.Length);
            GameObject drop = (GameObject)Instantiate(this.waterDrop, this.waterSpawns[spawnID].position, Quaternion.identity);
            drop.transform.SetParent(this.transform);
            drop.transform.localScale = new Vector3(0.3f,0.3f,0.3f);
        }
        waterTime = Time.time;
    }

    private void OnMouseUp() {
        if(BathroomManager.instance.IsTeethBrushed()) return;
        this.mover.MoveTo(BathroomManager.instance.GetStartPosition(), BathroomManager.instance.GetSpeed());
        if (SoundsManager.instance.GetOtherSoundsAudioSource().isPlaying) {
            SoundsManager.instance.GetOtherSoundsAudioSource().Stop();
        }
    }

    public Mover GetMover() {
        return this.mover;
    }
}
