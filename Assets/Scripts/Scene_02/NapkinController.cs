﻿using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

public class NapkinController : MonoBehaviour {
    public delegate void NapkinEvents();

    public event NapkinEvents Activate;
    public event NapkinEvents Deactivate;
    public event NapkinEvents Wiped;
    private Mover mover;
    private float timer;
    private bool activateTimer;

    private void OnEnable() {
        if (this.mover == null) {
            this.mover = GetComponent<Mover>();
        }
        this.timer = BathroomManager.instance.GetNapkinTimer();
    }

    private void OnDisable() {

    }

    private void Update() {
        if (this.activateTimer) {
            if (this.timer > 0f) {
                this.timer -= 1*Time.deltaTime;
            }
            else {
                timer = 0f;
                if (this.Wiped != null) {
                    this.Wiped();
                }
                this.activateTimer = false;
            }
        }
    }

    private void OnTriggerEnter(Collider col) {
        if (col.GetComponent<WaterDrop>()) {
            Destroy(col.gameObject);
        }
        if (col.name == "head") {
            SoundsManager.instance.PlayOtherSound("napkin");
        }
    }

    private void OnMouseDown() {
        this.activateTimer = true;
    }

    private void OnMouseUp() {
        this.activateTimer = false;
        if(BathroomManager.instance.IsTeethBrushed()) return;
        this.mover.MoveTo(BathroomManager.instance.GetStartPosition(), BathroomManager.instance.GetSpeed());
    }

    public Mover GetMover() {
        return this.mover;
    }


}
