﻿using UnityEngine;
using System.Collections;

public class Drop : MonoBehaviour {

    private void Start() {
        Destroy(this.gameObject, 1f);
    }

    private void Update() {
        transform.Translate((Vector3.left+Vector3.down)*Time.deltaTime*15f);
    }

    private void OnTriggerEnter(Collider col) {
        if (col.name == "Bubble") {
            BathroomManager.instance.RemoveBubbleList(col.gameObject);
            Destroy(col.gameObject);
            Destroy(this.gameObject, 0.1f);
        }
    }
}
