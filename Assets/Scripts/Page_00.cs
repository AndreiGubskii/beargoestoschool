﻿using UnityEngine;
using System.Collections;

public class Page_00 : Page {

    [SerializeField] private TouchHandler playBtn;
    private new void OnEnable(){
        base.OnEnable();
        if (SoundsManager.instance.GetVoiceAudioSource().isPlaying) {
            SoundsManager.instance.GetVoiceAudioSource().Stop();
        }
        playBtn.PointerDownEvent += this.PointerDownListener;
        playBtn.PointerUpEvent += this.PointerUpListener;
        playBtn.OnClick += this.ClickOnPlayListener;
        if (UICanvasController.instance != null) {
            UICanvasController.instance.ShowMiniGamesBtn();
        }
    }

    private void PointerUpListener(GameObject gameobject) {
        SoundsManager.instance.PlayOtherSound("click_up");
        gameobject.transform.localScale /= 1.1f;
    }

    private void PointerDownListener(GameObject gameobject) {
        SoundsManager.instance.PlayOtherSound("click_Down");
        gameobject.transform.localScale *= 1.1f;
    }

    private new void OnDisable(){
        base.OnDisable();
        playBtn.PointerDownEvent -= this.PointerDownListener;
        playBtn.PointerUpEvent -= this.PointerUpListener;
        playBtn.OnClick -= this.ClickOnPlayListener;
    }

    private void ClickOnPlayListener(GameObject gameobject) {
        PageChanger.instance.NextPage(0.3f);
    }
}
