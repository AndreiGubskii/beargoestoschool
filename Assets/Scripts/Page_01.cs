﻿using UnityEngine;
using System.Collections;

public class Page_01 : Page {

    private new void OnEnable(){
        base.OnEnable();
        this.text.text = LocalizationManager.Instance.LoadResource("Scene_01/Page_01").text;
        UICanvasController.instance.HideMiniGamesBtn();

        SoundsManager.instance.PlayVoice("1");
    }

    private new void OnDisable(){
        base.OnDisable();
    }
}
