﻿using UnityEngine;
using System.Collections;

public class Head02AnimationController : MonoBehaviour {
    private Animator _animator;

    private void OnEnable() {
        if (this._animator == null) {
            this._animator = GetComponent<Animator>();
        }
    }

    public void PlayOpenEyes() {
        this._animator.SetTrigger("openEyes");
    }
}
