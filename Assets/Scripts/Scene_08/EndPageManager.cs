﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndPageManager : MonoBehaviour {
    [SerializeField] private TouchHandler[] btns;
    [SerializeField] private GameObject[] firstPanel;
    [SerializeField] private GameObject[] secondPanel;
    private void OnEnable() {
        SoundsManager.instance.Play(
            SoundsManager.instance.GetBackMusicAudioSource(),
            SoundsManager.instance.GetAudioFromResources("Sounds/BackSounds/Scene_01/start_sound"),
            true);
        foreach (TouchHandler btn in btns) {
            btn.PointerDownEvent += OnPointerDownListener;
            btn.PointerUpEvent += OnPointerUpListener;
        }
        StartCoroutine(this.ManageIEnumerator());
    }

    private void OnDisable() {
        foreach (TouchHandler btn in btns) {
            btn.PointerDownEvent -= OnPointerDownListener;
            btn.PointerUpEvent -= OnPointerUpListener;
        }
    }

    private void OnPointerDownListener(GameObject btn) {
        btn.transform.localScale *= 1.1f;
    }

    private void OnPointerUpListener(GameObject btn) {
        btn.transform.localScale /= 1.1f;
        if (MainManager.instance != null) {
            MainManager.instance.LoadMiniGame(btn.GetComponent<BtnController>());
        }
    }

    private IEnumerator ManageIEnumerator() {
        this.ActivateFirstPanel(this.firstPanel,true);
        this.ActivateFirstPanel(this.secondPanel,false);
        AudioClip voice = SoundsManager.instance.GetVoiceByName("41");
        SoundsManager.instance.PlayVoice(voice);
        yield return new WaitForSeconds(voice.length + 1f);
        this.ActivateFirstPanel(this.firstPanel, false);
        this.ActivateFirstPanel(this.secondPanel, true);

        AudioClip voice2 = SoundsManager.instance.GetVoiceByName("40");
        SoundsManager.instance.PlayVoice(voice2);
        StartCoroutine(AnimationBtnsIEnumerator(voice2.length - 3.5f, voice2.length - 3.3f));
    }

    private IEnumerator AnimationBtnsIEnumerator(float firstDelay, float secondDelay) {
        yield return new WaitForSeconds(firstDelay);
        this.secondPanel[0].GetComponent<Animator>().SetTrigger("returnBtn");
        yield return new WaitForSeconds(secondDelay);
        this.secondPanel[0].GetComponent<Animator>().SetTrigger("icon");
        yield return new WaitForSeconds(3f);
        StartCoroutine(AnimationBtnsIEnumerator(firstDelay, secondDelay));
    }

    private void ActivateFirstPanel(GameObject[] panel,bool active) {
        foreach (GameObject o in panel) {
            o.SetActive(active);
        }
    }

}
