﻿using UnityEngine;
using System.Collections;

public class BtnController : MonoBehaviour {
    [SerializeField] private ObjectsTypes.BtnType type;
    [SerializeField] private int sceneId;
    public ObjectsTypes.BtnType GetBtnType() {
        return this.type;
    }

    public int GetSceneID() {
        return this.sceneId;
    }
}
