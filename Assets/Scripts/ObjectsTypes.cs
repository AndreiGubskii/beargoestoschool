﻿using UnityEngine;
using System.Collections;

public class ObjectsTypes {
    public enum Clothing {
        DEFAULT,
        PANTS,
        CAP,
        SHOE,
        BACKPACK,
        SINGLET
    }

    public enum Food {
        DEFAULT,
        EATABLE,
        INEDIABLE
    }

    public enum SchoolSuppliesName {
        DEFAULT,
        NOTEBOOK,
        DUCK,
        BRICKS,
        DINO,
        AIRCRAFT,
        PEN,
        BOOKS,
        PENCIL,
        RULER,
        CAR

    }

    public enum SchoolSuppliesType {
        DEFAULT,
        FORSHOOL,
        NOTFORSHOOL
    }

    public enum Direction {
        DEFAULT,
        FORWARD,
        BACK,
        RIGHT,
        LEFT
    }

    public enum BtnType {
        DEFAULT,
        PLAY,
        RETURN,
        MINI_GAME
    }
}
