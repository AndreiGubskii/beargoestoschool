﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {
    [SerializeField] private GameObject exp;
    private void OnTriggerEnter(Collider col) {
        col.GetComponent<CatController>().CatInTrigger();
        GameObject e = Instantiate(this.exp);
        e.transform.SetParent(this.transform.parent);
        e.transform.localPosition = this.transform.localPosition;
        e.transform.localScale = Vector3.one;
        Destroy(e,1f);
        this.gameObject.SetActive(false);
    }
}
