﻿using System;
using UnityEngine;
using System.Collections;

public class CatController : MonoBehaviour {
    public delegate void CatEvents();
    public event CatEvents MissedCat;
    public event CatEvents ChasedCat;
    [SerializeField] private float speed;
    [SerializeField] private Vector3 leftTarget;
    [SerializeField] private Vector3 rightTarget;
    [SerializeField] private Vector3 birdPosition;
    private float boundary = 900f;
    private Vector3 target;
    private Vector3 endTarget;
    private TouchHandler touchHandler;

    private void Awake() {
        this.touchHandler = GetComponent<TouchHandler>();
    }

    private void Start() {
        this.touchHandler.PointerUpEvent += this.PointerUpListner;
        this.touchHandler.PointerDownEvent += this.PointerDownListner;
        this.leftTarget = new Vector3(
            this.leftTarget.x,
            this.leftTarget.y,
            this.transform.localPosition.z);
        this.rightTarget = new Vector3(
            this.rightTarget.x,
            this.transform.localPosition.y,
            this.transform.localPosition.z);

        this.target = this.birdPosition;
        if (this.transform.localPosition.x < 0) {
            this.endTarget = this.leftTarget;
            this.transform.localScale = new Vector3(-1,1,1);
        }
        else {
            this.endTarget = this.rightTarget;
            this.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    private void Update() {
        if (SaveBirdManager.instance.IsMissed()) {
            GetComponent<Animator>().speed = 0;
            return;
        }
        
        this.transform.localPosition = Vector3.MoveTowards(
            this.transform.localPosition,
            this.target,
            this.speed * Time.deltaTime);

        if (Math.Abs(this.transform.localPosition.x - this.endTarget.x) < 1f) {
            Destroy(this.gameObject);
        }
    }

    private void PointerDownListner(GameObject gameobject) {
            if (SaveBirdManager.instance.IsMissed()) return;

            if (this.target != this.endTarget) {
            this.target = this.endTarget;
            this.transform.localScale = new Vector3(this.transform.localScale .x * -1,1,1);
            if (this.ChasedCat != null) {
                this.ChasedCat();
            }
            SoundsManager.instance.PlayOtherSound("whip");
        }
    }

    private void PointerUpListner(GameObject gameobject) {

    }

    public void SetSpeed(float speed) {
        this.speed = speed;
    }

    public void CatInTrigger() {
        if (this.MissedCat != null) {
            this.MissedCat();
        }
        Destroy(this.gameObject);
    }
}
