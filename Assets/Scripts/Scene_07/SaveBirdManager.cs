﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SaveBirdManager : MonoBehaviour {
    public static SaveBirdManager instance;
    [SerializeField] private GameObject cat;
    [SerializeField] private float setRepeatTime;
    [SerializeField] private float maxRepeatTime;
    [SerializeField] private float startCatSpeed;
    [SerializeField] private float maxCatSpeed;
    [SerializeField] private Transform parentPanel;
    [SerializeField] private Vector3[] startPosition;
    [SerializeField] private UIBearAnimationController bearAnimation;
    [SerializeField] private int countChasedCat;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private GameObject bird;
    private int counterChasedCat;
    private float startTime;
    private bool end;
    private bool missed;
    private int counterCat;
    
    private float catSpeed;
    private float repeatTime;
    private int complexityThrough = 5;
    private int complexityThroughCounter;
    private void Awake() {
        instance = this;
    }

    void Start () {
	    this.Restart();
	}
	
	void Update () {
	    if (!this.end && (this.startTime + this.repeatTime) < Time.time) {
            CreateCat();
        }

	    if (!end && (this.countChasedCat <= this.counterChasedCat)) {
	        StartCoroutine(this.End(3f));
	    }
	}

    private void CreateCat() {
        if(this.counterCat >= this.countChasedCat || this.IsMissed()) return;
        this.counterCat++;
        this.startTime = Time.time;
        GameObject obj = Instantiate(this.cat);
        obj.transform.SetParent(this.parentPanel);
        int i = Random.Range(0, startPosition.Length);
        obj.transform.localPosition = this.startPosition[i];
        obj.transform.localScale = Vector3.one;
        obj.GetComponent<CatController>().ChasedCat += this.ChasedCatListener;
        obj.GetComponent<CatController>().MissedCat += this.MissedCatListener;
        obj.GetComponent<CatController>().SetSpeed(this.catSpeed);
        this.complexityThroughCounter++;
        if (this.complexityThroughCounter >= this.complexityThrough) {
            this.complexityThroughCounter = 0;
            if (this.repeatTime > this.maxRepeatTime) {
                this.repeatTime /= 1.1f;
            }
            if (this.catSpeed < this.maxCatSpeed) {
                this.catSpeed *= 1.1f;
            }

        }
    }

    private void MissedCatListener() {
        this.bearAnimation.PlaySadAnimation();
        this.missed = true;
        MainManager.instance.ActivateRestartPanel();
        SoundsManager.instance.PlayBackSound("gameover", false);
    }

    private void ChasedCatListener() {
        this.bearAnimation.PlaySmileAnimaton();
        if (this.countChasedCat > this.counterChasedCat) {
            this.counterChasedCat++;
        }
    }

    private IEnumerator End(float delay) {
        this.end = true;
        yield return new WaitForSeconds(delay);
        this.endPanel.SetActive(true);
        SoundsManager.instance.GetBackMusicAudioSource().Stop();
        AudioClip voice = SoundsManager.instance.GetVoiceByName("38");
        SoundsManager.instance.PlayVoice(voice);
        this.bearAnimation.PlaySmileOnce();
        yield return new WaitForSeconds(voice.length);
        if (MainManager.instance != null) {
            MainManager.instance.ShowEndPanel(0f, 0.2f, SoundsManager.instance.GetVoiceByName("39"));
        }
    }

    public bool IsMissed() {
        return this.missed;
    }

    public void Restart() {
        SoundsManager.instance.PlayBackSound("savebird_sound");
        AudioClip voice = SoundsManager.instance.GetVoiceByName("37");
        SoundsManager.instance.PlayVoice(voice);
        this.end = false;
        this.missed = false;
        this.counterChasedCat = 0;
        this.counterCat = 0;
        this.bird.SetActive(true);
        this.endPanel.SetActive(false);
        this.catSpeed = this.startCatSpeed;
        this.repeatTime = this.setRepeatTime;
        this.bearAnimation.Reset();
        CatController[] cats = FindObjectsOfType<CatController>();
        foreach (CatController catController in cats) {
            Destroy(catController.gameObject);
        }
        this.startTime = Time.time + (voice.length-2.5f);
    }
}
