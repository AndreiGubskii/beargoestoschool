﻿using System;
using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {
    public delegate void MoverEvents();
    public event MoverEvents OnBackToStartPosition;
    private Vector3 target;
    private float speed;
    private bool move;
    private Vector3 startPosition;
    private void OnEnable() {
        this.startPosition = this.transform.localPosition;
        this.move = false;
    }

    private void Update() {
        if (this.move) {
            if (this.transform.localPosition != this.target) {
                this.transform.localPosition =
                    Vector3.MoveTowards(this.transform.localPosition, this.target, this.speed*Time.deltaTime);
            }
            else {
                if (Vector3.Distance(this.transform.localPosition,this.startPosition) < 0.1f) {
                    if (this.OnBackToStartPosition != null) {
                        this.OnBackToStartPosition();
                    }
                }
                this.move = false;
            }
        }
    }

    public void MoveTo(Vector3 target,float speed) {
        this.target = target;
        this.speed = speed;
        this.move = true;
    }

    public void MoveTo(float delay, Vector3 target, float speed) {
        StartCoroutine(this.MoveToIEnumerator(delay, target, speed));
    }

    private IEnumerator MoveToIEnumerator(float delay, Vector3 target, float speed) {
        yield return new WaitForSeconds(delay);
        this.MoveTo(target,speed);
    }
}
