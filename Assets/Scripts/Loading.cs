﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading : MonoBehaviour {

	public static int level = 2;
    [SerializeField] private Text loadingText;

    private void Awake() {
        SoundsManager.instance.GetVoiceAudioSource().Stop();
        SoundsManager.instance.GetBackMusicAudioSource().Stop();
    }

    private IEnumerator Start() {
        yield return new WaitForSeconds(0.5f);
		AsyncOperation async = SceneManager.LoadSceneAsync(level);
		while( !async.isDone ) {
		    this.loadingText.text = string.Format("Loading {0}%", async.progress*100);
			yield return null;
		}

		yield return async;
	}
	
}
