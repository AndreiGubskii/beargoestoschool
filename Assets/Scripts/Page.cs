﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Page : MonoBehaviour {
    public delegate void UICanvasInstanceCallback();
    [System.Serializable]
    public class ButtonsControll {
        public bool showNextBtn;
        public bool showPreviousBtn;
        public bool showLoadSceneBtn;
        public bool showTextPanel;
    }

    [SerializeField] protected Text text;
    [SerializeField] protected Image background;
    [SerializeField] protected string backgroundName;
    [SerializeField] private ButtonsControll buttonsControll;
    protected void OnEnable() {
        StartCoroutine(this.GetUICanvasIEnumerator(this.ButtonsSet));
    }

    protected void OnDisable() {
    }

    private IEnumerator GetUICanvasIEnumerator(UICanvasInstanceCallback callback) {
        while (UICanvasController.instance == null) {
            yield return new WaitForEndOfFrame();
        }

        if (callback != null) {
            callback();
        }
    }

    private void ButtonsSet() {
        if (this.buttonsControll.showNextBtn) {
            UICanvasController.instance.ShowNextBtn();
        }
        else {
            UICanvasController.instance.HideNextBtn();
        }

        if (this.buttonsControll.showPreviousBtn) {
            UICanvasController.instance.ShowPreviousBtn();
        }
        else {
            UICanvasController.instance.HidePreviousBtn();
        }

        if (this.buttonsControll.showLoadSceneBtn) {
            UICanvasController.instance.ShowLoadSceneBtn();
        }
        else {
            UICanvasController.instance.HideLoadSceneBtn();
        }

        if (this.buttonsControll.showTextPanel) {
            UICanvasController.instance.ShowTextPanel();
        }
        else {
            UICanvasController.instance.HideTextPanel();
        }
    }

}
