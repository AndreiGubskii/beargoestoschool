﻿using UnityEngine;
using System.Collections;
#if !UNITY_WP_8_1 && !UNITY_WINRT
using UnityEngine.Advertisements;
#endif
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour {
    [System.Serializable] public class AdsSettings {
        public bool enable = true;
        public int maximumImpressions;

        private int counterImpressions;
        private bool adsShowed;

        public bool GetAdsShowed() {
            return this.adsShowed;
        }

        public void ResetAdsShowed() {
            this.adsShowed = false;
        }

        public void CountImpressions() {
            this.counterImpressions++;
            this.adsShowed = true;
        }

        public void ResetCounterImpressions() {
            this.counterImpressions = 0;
        }

        public int GetCountImpressions() {
            return this.counterImpressions;
        }
    }
    [System.Serializable]
    public class WP_Ads
    {
        public string appId;
        public string videoAdId;
    }

    [System.Serializable]
    public class Buttons {
        public TouchHandler returnBtn;
        public TouchHandler nextSceneBtn;
        public TouchHandler homeBtn;
    }

    public static MainManager instance;
    private float numMultScale = 1.2f;
    private bool onlyGame;
    private float delayBeforeLoading = 0.3f;
    [SerializeField] private AdsSettings adsSettings;
    [SerializeField] private WP_Ads wp_Ads;
    [SerializeField] private Buttons buttons;
    [SerializeField] private GameObject endPanel;
    
    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        this.onlyGame = false;
        this.buttons.homeBtn.gameObject.SetActive(false);
        this.buttons.nextSceneBtn.gameObject.SetActive(false);
        buttons.returnBtn.PointerDownEvent += this.PointerDownReturnBtnListener;
        buttons.returnBtn.PointerUpEvent += this.PointerUpReturnBtnListener;
        buttons.nextSceneBtn.PointerUpEvent += this.PointerUpNextSceneBtnListener;
        buttons.nextSceneBtn.PointerDownEvent += this.PointerDownNextSceneBtnListener;
        buttons.homeBtn.PointerUpEvent += this.PointerUpHomeBtnListener;
        buttons.homeBtn.PointerDownEvent += this.PointerDownHomeBtnListener;
    }

    private void Start() {
        this.adsSettings.ResetCounterImpressions();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }


    public void ActivateRestartPanel() {
        this.buttons.nextSceneBtn.gameObject.SetActive(false);
        this.buttons.homeBtn.gameObject.SetActive(false);
        this.buttons.returnBtn.transform.localPosition = new Vector3(0f, 0f, 0f);
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.one);
    }

    public void DeactivateRestartPanel() {
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.zero);
    }

    public void ActivateEndPanel() {
        this.buttons.nextSceneBtn.gameObject.SetActive(true);
        this.buttons.homeBtn.gameObject.SetActive(false);
        this.buttons.returnBtn.transform.localPosition = new Vector3(-170f,0f,0f);
        this.buttons.nextSceneBtn.transform.localPosition = new Vector3(170f, 0f, 0f);
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.one);
    }

    public void DeactivateEndPanel() {
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.zero);
    }

    public void ActivateEndWithHomeBtnPanel() {
        this.buttons.nextSceneBtn.gameObject.SetActive(false);
        this.buttons.homeBtn.gameObject.SetActive(true);
        this.buttons.returnBtn.transform.localPosition = new Vector3(-170f, 0f, 0f);
        this.buttons.homeBtn.transform.localPosition = new Vector3(170f, 0f, 0f);
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.one);
    }

    public void DeactivateEndWithHomeBtnPanel() {
        this.endPanel.GetComponent<Scaler>().ScaleTo(Vector3.zero);
    }

    public void LoadMiniGame(BtnController btnController) {
        this.LoadLevel(btnController.GetSceneID());

        if (btnController.GetBtnType() == ObjectsTypes.BtnType.MINI_GAME) {
            this.onlyGame = true;
        }
    }

    private void LoadLevel(int sceneID) {
        Loading.level = sceneID;
        SceneManager.LoadScene("Loading");
    }

    private void LoadLevel(int sceneID,float delay) {
        StartCoroutine(LoadLevelIEnumerator(sceneID, delay));
    }

    private IEnumerator LoadLevelIEnumerator(int sceneID,float delay) {
        yield return new WaitForSeconds(delay);
        this.LoadLevel(sceneID);
    }

    private void PointerDownHomeBtnListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        this.ScaleBtnMultiply(btn.transform, this.numMultScale);
    }

    private void PointerUpHomeBtnListener(GameObject btn) {
        this.ScaleBtnSplit(btn.transform, this.numMultScale);
        this.LoadLevel(2, this.delayBeforeLoading);
        DeactivateEndWithHomeBtnPanel();
        this.ResetOnlyGameVariable();
    }

    private void PointerDownReturnBtnListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        this.ScaleBtnMultiply(btn.transform,this.numMultScale);
    }

    private void PointerUpReturnBtnListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        this.ScaleBtnSplit(btn.transform, this.numMultScale);
        string sceneName = SceneManager.GetActiveScene().name;

        switch (sceneName) {
            case "Scene_02":
                BathroomManager.instance.Restart();
                break;
            case "Scene_03":
                ClothingSearch.instance.Restart();
                break;
            case "Scene_04":
                KitchenManager.instance.Restart();
                break;
            case "Scene_05":
                SearchSchoolSupplies.instance.Restart();
                break;
            case "Scene_06":
                LabyrinthManager.instance.Restart();
                break;
            case "Scene_07":
                SaveBirdManager.instance.Restart();
                break;
        }
        this.DeactivateRestartPanel();
    }

    private void PointerDownNextSceneBtnListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_down");
        this.ScaleBtnMultiply(btn.transform, this.numMultScale);
    }

    private void PointerUpNextSceneBtnListener(GameObject btn) {
        SoundsManager.instance.PlayOtherSound("click_up");
        this.ScaleBtnSplit(btn.transform, this.numMultScale);
        this.LoadLevel(SceneManager.GetActiveScene().buildIndex + 1,this.delayBeforeLoading);
        this.DeactivateEndPanel();
    }

    private void ScaleBtnMultiply(Transform btn, float number) {
        btn.localScale *= number;
    }

    private void ScaleBtnSplit(Transform btn, float number) {
        btn.localScale /= number;
    }

    public void ResetOnlyGameVariable() {
        this.onlyGame = false;
    }

    public bool GetOnlyGameVariable() {
        return this.onlyGame;
    }

    public void ShowEndPanel(float delay, float delayBeforeVoiceEnd, AudioClip voiceEnd = null) {
        StartCoroutine(this.ShowEndPanelIEnumerator(delay,delayBeforeVoiceEnd ,voiceEnd));
        int i = Random.Range(0, 2);
        if (i > 0) {
            this.ShowAds();
        }
    }

    private IEnumerator ShowEndPanelIEnumerator(float delay, float delayBeforeVoiceEnd,AudioClip voiceEnd = null) {
        yield return new WaitForSeconds(delay);
            if (GetOnlyGameVariable()) {
                ActivateEndWithHomeBtnPanel();
            }
            else {
                //ActivateEndPanel();
                if (SoundsManager.instance != null && voiceEnd != null) {
                    SoundsManager.instance.PlayVoice(voiceEnd.name, delayBeforeVoiceEnd);
                }
                float length = 0.5f;
                if (voiceEnd != null) {
                    length = voiceEnd.length;
                }
                this.LoadLevel(SceneManager.GetActiveScene().buildIndex+1,length+1.5f);
            }
    }

#if !UNITY_WP_8_1 && !UNITY_WINRT
    private IEnumerator ShowAdWhenReady() {
        while (!Advertisement.IsReady())
            yield return null;

        Advertisement.Show();
        this.adsSettings.CountImpressions();
    }
#endif
    
    public void ShowAds() {
        if (this.adsSettings.enable) {
            if (adsSettings.GetCountImpressions() >= adsSettings.maximumImpressions) {
                return;
            }
        }
        if (!this.adsSettings.GetAdsShowed()) {
            #if !UNITY_WP_8_1 && !UNITY_WINRT
                    StartCoroutine(this.ShowAdWhenReady());
            #endif
            #if UNITY_WP_8_1 && !UNITY_IOS && !UNITY_ANDROID
                    Interop.ShowInterstitialAd(wp_Ads.appId,wp_Ads.videoAdId);
            #endif
        }
        else {
            this.adsSettings.ResetAdsShowed();
        }
    }
}
