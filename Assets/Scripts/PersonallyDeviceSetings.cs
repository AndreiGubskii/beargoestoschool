﻿using UnityEngine;
#if UNITY_IOS && (!UNITY_ANDROID || !UNITY_WSA)
using UnityEngine.iOS;
#endif
public class PersonallyDeviceSetings : MonoBehaviour {
    public delegate void CallBackDelegate();
    public enum MyDeviceType {
        DEFAULT,
        ANDROID,
        IPAD,
        WINDOWSPHONE
    }
    [System.Serializable]
    public class Objs {
        public GameObject obj;

        public Vector3 position;
        public Vector3 scale;
    }

    [System.Serializable]public class DebugSetings {
        public bool debuging = false;
        public MyDeviceType MyDeviceType;
    }

    [SerializeField] private DebugSetings debugSetings;
    [SerializeField] private Objs[] iPadSetings;
    [SerializeField] private Objs[] androidSetings;
    [SerializeField] private Objs[] wsaSetings;
    private MyDeviceType deviceType;
    private void OnEnable() {
        this.AutoSetDeviceType(this.ApplyDeviceSetings);
    }

    private void ApplyDeviceSetings() {
        switch (this.deviceType) {
            case MyDeviceType.ANDROID:
                foreach (Objs objsSeting in androidSetings){
                    objsSeting.obj.transform.localPosition = objsSeting.position;
                    objsSeting.obj.transform.localScale = objsSeting.scale;
                }
                break;
            case MyDeviceType.IPAD:
                foreach (Objs objsSeting in iPadSetings){
                    objsSeting.obj.transform.localPosition = objsSeting.position;
                    objsSeting.obj.transform.localScale = objsSeting.scale;
                }
                break;
            case MyDeviceType.WINDOWSPHONE:
                foreach (Objs objsSeting in wsaSetings){
                    objsSeting.obj.transform.localPosition = objsSeting.position;
                    objsSeting.obj.transform.localScale = objsSeting.scale;
                }
                break;
        }
    }

    private void AutoSetDeviceType(CallBackDelegate callBack) {
        if (this.debugSetings.debuging) {
            this.deviceType = this.debugSetings.MyDeviceType;
        }else {
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
            if (Device.generation.ToString().IndexOf("iPad") > -1) {
                this.deviceType = MyDeviceType.IPAD;
            }
#endif

#if (!UNITY_IOS || !UNITY_ANDROID) && UNITY_WSA
            this.deviceType = MyDeviceType.WINDOWSPHONE;
#endif

#if (!UNITY_IOS || !UNITY_WSA) && UNITY_ANDROID
            this.deviceType = MyDeviceType.ANDROID;
#endif
        }
        if (callBack != null) {
            callBack();
        }
    }
    
}
