using UnityEngine;
using System.Collections;
using System;

public static class Interop {
    public delegate void InteropEvents(string myAppId, string myAdUnitId);

    public static event InteropEvents ShowAd;

    public static void LoadInterstitialAd() {

    }

    public static void ShowInterstitialAd(string myAppId, string myAdUnitId) {
        if (null != ShowAd) {
            ShowAd(myAppId,myAdUnitId);
        }
    }

    public static void InterstitialClose() {

    }
}