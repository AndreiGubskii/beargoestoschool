﻿using UnityEngine;
using System.Collections;

public class LabyrinthBearController : MonoBehaviour {
    private Vector3[] path;
    private int pathId = 0;
    private bool move;
    private LabyrinthBaerAnimationController animationController;
    private bool turnBacks;

    [SerializeField] private float speed;

    private void Update() {
        if (this.move && this.path.Length!=0) {
           if(!this.turnBacks && this.path[this.pathId].y > (this.transform.localPosition.y+5)){
                this.GetAnimationController().PlayBackWalk();
               this.turnBacks = true;
           }

            if (this.transform.localPosition != this.path[this.pathId]) {
                this.transform.localPosition = Vector3.MoveTowards(
                    this.transform.localPosition,
                    this.path[this.pathId],
                    this.speed*Time.deltaTime
                    );
            }
            else {
                if (this.pathId < path.Length) {
                    this.pathId++;
                }
                if (this.pathId == path.Length) {
                    this.ResetPathId();
                    this.move = false;
                    this.turnBacks = false;
                }
            }
        }
    }

    public void MoveTo(Vector3[] direction) {
        this.ResetPathId();
        this.path = direction;
        this.move = true;
    }

    public void StopMove() {
        this.ResetPathId();
        this.move = false;
        this.turnBacks = false;
    }

    public LabyrinthBaerAnimationController GetAnimationController() {
        if (this.animationController == null) {
            this.animationController = GetComponent<LabyrinthBaerAnimationController>();
        }
        return this.animationController;
    }

    public void ResetPathId() {
        this.pathId = 0;
    }
}
