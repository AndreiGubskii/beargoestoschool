﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class LabyrinthBtnDirection : MonoBehaviour,IPointerDownHandler,IPointerUpHandler {
    public delegate void LabyrinthBtnDirectionEvents(ObjectsTypes.Direction direction);

    public event LabyrinthBtnDirectionEvents OnPointerUpEvent;
    public event LabyrinthBtnDirectionEvents OnPointerDownEvent;
    private bool _acive;
    [SerializeField] private ObjectsTypes.Direction type;

    public void OnPointerDown(PointerEventData eventData) {
        if(!_acive)return;
        if (this.OnPointerDownEvent != null) {
            this.OnPointerDownEvent(this.type);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (!_acive) return;
        if (this.OnPointerUpEvent != null) {
            this.OnPointerUpEvent(this.type);
        }
    }

    public ObjectsTypes.Direction GetType() {
        return this.type;
    }

    public void SetActive(bool active) {
        this._acive = active;
    }
}
