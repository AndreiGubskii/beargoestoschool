﻿using UnityEngine;
using System.Collections;

public class PointController : MonoBehaviour {
    public delegate void PointEvents(Trajectory[] trajectory);
    public event PointEvents OnTriggerEnterEvent;
    [System.Serializable] public class Trajectory {
        public Vector3[] trajectory;
        public ObjectsTypes.Direction direction;
    }
    [SerializeField] private string voiceName;
    [SerializeField] private Trajectory[] trajectoryWays;

    private void OnTriggerEnter(Collider col) {
        if (this.OnTriggerEnterEvent != null) {
            this.OnTriggerEnterEvent(this.trajectoryWays);
        }

        if (voiceName != "") {
            SoundsManager.instance.PlayVoice(voiceName);
        }
    }
}
