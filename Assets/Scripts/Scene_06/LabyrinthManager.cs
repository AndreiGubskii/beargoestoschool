﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LabyrinthManager : MonoBehaviour {
    [System.Serializable] public class Labyrinths {
        public string backImagePath;
        public PointController[] points;
        public PointController.Trajectory[] startTrajectory;
        public GameObject pointsPanel;
    }

    public static LabyrinthManager instance;
    [SerializeField] private LabirinthShool shool;
    [SerializeField] private Labyrinths[] labyrinths;
    [SerializeField] private LabyrinthBtnDirection[] buttons;
    [SerializeField] private LabyrinthBearController bearController;
    [SerializeField] private Image backImage;
    [SerializeField] private PointController.Trajectory[] startTrajectory;
    [SerializeField] private Vector3 bearStartPosition;
    private PointController.Trajectory[] trajectory;
    private Vector3[] path;
    private int labyrinthID;

    private void Awake() {
        instance = this;
    }

    void Start () {
        this.Restart();
        foreach (LabyrinthBtnDirection button in buttons) {
            button.OnPointerUpEvent += OnUpDirectionBtn;
            button.OnPointerDownEvent += OnDownkDirectionBtn;
        }
	}

    private void OnTriggerEnterListener(PointController.Trajectory[] trajectory) {
        this.SetTrajectory(trajectory);
        this.bearController.GetAnimationController().PlayIdle();
    }


    private void OnDownkDirectionBtn(ObjectsTypes.Direction direction) {
        this.bearController.MoveTo(this.GetPath(direction));
        switch (direction) {
            case ObjectsTypes.Direction.FORWARD:
                this.bearController.GetAnimationController().PlayBackWalk();
                break;
            case ObjectsTypes.Direction.BACK:
                this.bearController.GetAnimationController().PlayFaceWalk(direction);
                break;
            case ObjectsTypes.Direction.LEFT:
                this.bearController.GetAnimationController().PlayFaceWalk(direction);
                break;
            case ObjectsTypes.Direction.RIGHT:
                this.bearController.GetAnimationController().PlayFaceWalk(direction);
                break;
        }
        this.HideAllBtns();
    }

    private void OnUpDirectionBtn(ObjectsTypes.Direction direction) {
    }

    private Vector3[] GetPath(ObjectsTypes.Direction direction) {
        foreach (PointController.Trajectory tr in this.trajectory) {
            if (tr.direction == direction) {
                return tr.trajectory;
            }
        }
        return null;
    }

    private LabyrinthBtnDirection GetBtnByType(ObjectsTypes.Direction btnType) {
        foreach (LabyrinthBtnDirection btn in buttons) {
            if (btn.GetType() == btnType) {
                return btn;
            }
        }
        return null;
    }

    private void HideAllBtns() {
        foreach (LabyrinthBtnDirection btn in buttons) {
           btn.gameObject.GetComponent<LabyrinthBtnDirection>().SetActive(false);
        }
    }

    private void SetTrajectory(PointController.Trajectory[] trajectory) {
        this.trajectory = trajectory;
        foreach (LabyrinthBtnDirection button in buttons) {
            button.gameObject.GetComponent<LabyrinthBtnDirection>().SetActive(false);
        }
        foreach (PointController.Trajectory tr in trajectory) {
            GetBtnByType(tr.direction).gameObject.GetComponent<LabyrinthBtnDirection>().SetActive(true);
        }
    }

    private void BearInShoolListener() {
        Scaler scaler = this.bearController.gameObject.GetComponent<Scaler>();
        if (scaler == null) {
            scaler = this.bearController.gameObject.AddComponent<Scaler>();
        }
        scaler.ScaleTo(Vector3.zero);

        this.labyrinths[this.labyrinthID].pointsPanel.SetActive(false);
        this.shool.InShool -= this.BearInShoolListener;
        for (int i = 0; i < this.labyrinths[this.labyrinthID].points.Length; i++) {
	        this.labyrinths[this.labyrinthID].points[i].OnTriggerEnterEvent -= OnTriggerEnterListener;
	    }
        AudioClip voice = SoundsManager.instance.GetVoiceByName("36");
        SoundsManager.instance.PlayVoice(voice);
        if (MainManager.instance != null) {
            MainManager.instance.ShowEndPanel(voice.length, 0f);
        }
    }



    public void Restart() {
        SoundsManager.instance.PlayBackSound("labyrinth_sound");
        this.shool.InShool += this.BearInShoolListener;
        this.labyrinthID = Random.Range(0, this.labyrinths.Length);
        this.backImage.sprite = Resources.Load<Sprite>(this.labyrinths[this.labyrinthID].backImagePath);
        this.labyrinths[this.labyrinthID].pointsPanel.SetActive(true);
        this.SetTrajectory(this.labyrinths[this.labyrinthID].startTrajectory);

        for (int i = 0; i < this.labyrinths[this.labyrinthID].points.Length; i++){
            this.labyrinths[this.labyrinthID].points[i].OnTriggerEnterEvent += OnTriggerEnterListener;
        }
        this.bearController.transform.localScale = Vector3.one;
        this.bearController.transform.localPosition = this.bearStartPosition;
        SoundsManager.instance.PlayVoice("31");

    }
}
