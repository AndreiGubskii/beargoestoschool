﻿using UnityEngine;
using System.Collections;

public class LabyrinthBaerAnimationController : MonoBehaviour {
    private Animator _animator;
    private Vector3 defaultScale;
    [SerializeField] private Transform imageTransform;
    private void Awake() {
        this._animator = GetComponent<Animator>();
        this.defaultScale = this.imageTransform.localScale;
    }

    public void PlayBackWalk() {
        this._animator.SetTrigger("backWalk");
        this._idle = false;
    }

    private bool _idle;
    public void PlayIdle() {
        if (!_idle) {
            this._animator.SetTrigger("idle");
            _idle = true;
        }
    }

    public void PlayFaceWalk(ObjectsTypes.Direction direction) {
        switch (direction) {
            case ObjectsTypes.Direction.LEFT:
                this.imageTransform.localScale = new Vector3(
                    -this.defaultScale.x,
                    this.defaultScale.y,
                    this.defaultScale.z
                    );
                break;
            case ObjectsTypes.Direction.RIGHT:
                this.imageTransform.localScale = this.defaultScale;
                break;
            default:
                this.imageTransform.localScale = this.defaultScale;
                break;
        }
        this._animator.SetTrigger("faceWalk");
        this._idle = false;
    }
}
