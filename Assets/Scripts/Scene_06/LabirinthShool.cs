﻿using UnityEngine;
using System.Collections;

public class LabirinthShool : MonoBehaviour {
    public delegate void ShoolEvents();
    public event ShoolEvents InShool;

    private void OnTriggerEnter() {
        if (this.InShool != null) {
            this.InShool();
        }
    }
}
