﻿using UnityEngine;
using System.Collections;

public class Page_03 : Page {
    private new void OnEnable(){
        base.OnEnable();
        this.text.text = LocalizationManager.Instance.LoadResource("Scene_01/Page_03").text;

        SoundsManager.instance.PlayVoice("3");
    }

    private new void OnDisable(){
        base.OnDisable();
    }
}
