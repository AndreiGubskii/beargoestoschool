﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TouchHandler : MonoBehaviour, IPointerClickHandler,
    IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, 
    IBeginDragHandler,IDragHandler,IEndDragHandler
{
    public delegate void TouchEvents(GameObject gameObject);

    public event TouchEvents OnClick;
    public event TouchEvents PointerEnterEvent;
    public event TouchEvents PointerExitEvent;
    public event TouchEvents PointerDownEvent;
    public event TouchEvents PointerUpEvent;
    public event TouchEvents BeginDragEvent;
    public event TouchEvents DragEvent;
    public event TouchEvents EndDragEvent;

    public void OnPointerClick(PointerEventData eventData) {
        if (this.OnClick != null) {
            this.OnClick(this.gameObject);
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (this.PointerEnterEvent != null) {
            this.PointerEnterEvent(this.gameObject);
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        if (this.PointerExitEvent != null) {
            this.PointerExitEvent(this.gameObject);
        }
    }

    public void OnPointerDown(PointerEventData eventData) {
        if (this.PointerDownEvent != null) {
            this.PointerDownEvent(this.gameObject);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (this.PointerUpEvent != null) {
            this.PointerUpEvent(this.gameObject);
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (this.BeginDragEvent != null) {
            this.BeginDragEvent(this.gameObject);
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if (this.DragEvent != null) {
            this.DragEvent(this.gameObject);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (this.EndDragEvent != null) {
            this.EndDragEvent(this.gameObject);
        }
    }
}
