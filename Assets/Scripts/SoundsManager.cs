﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SoundsManager : MonoBehaviour {

    public static SoundsManager instance;
    [SerializeField] private AudioSource voiceAudioSource;
    [SerializeField] private AudioSource backMusicAudioSource;
    [SerializeField] private AudioSource otherSoundsAudioSource;

    private void Awake() {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(this.voiceAudioSource.gameObject);
        DontDestroyOnLoad(this.backMusicAudioSource.gameObject);
        DontDestroyOnLoad(this.otherSoundsAudioSource.gameObject);
    }

    public void Stop(AudioSource audioSource) {
        if (audioSource.isPlaying) {
            audioSource.Stop();
        }
    }

    public void Play(AudioSource audioSource,AudioClip clip, bool loop) {
        if (audioSource != null) {
            audioSource.clip = clip;
            audioSource.loop = loop;
            audioSource.Play();
        }
    }

    public void Play(AudioSource audioSource, AudioClip clip,float delay, bool loop) {
        StartCoroutine(this.PlayIenumerator(audioSource, clip, delay, loop));
    }

    private IEnumerator PlayIenumerator(AudioSource audioSource, AudioClip clip,float delay, bool loop) {
        yield return new WaitForSeconds(delay);
        this.Play(audioSource, clip, loop);
    }

    public void PlayOneShot(AudioSource audioSource,AudioClip clip,float volume = 1f) {
        audioSource.PlayOneShot(clip,volume);
    }

    public AudioClip GetAudioFromResources(string path) {
        return Resources.Load<AudioClip>(path);
    }

    public AudioSource GetVoiceAudioSource() {
        return this.voiceAudioSource;
    }

    public AudioSource GetBackMusicAudioSource() {
        return this.backMusicAudioSource;
    }

    public AudioSource GetOtherSoundsAudioSource() {
        return this.otherSoundsAudioSource;
    }

    public void PlayVoice(string nameVoiceSound) {
        Play(GetVoiceAudioSource(),
            GetAudioFromResources(
                GetSoundVoicePath() + nameVoiceSound),
            false);
    }

    public void PlayVoice(string nameVoiceSound,float delay) {
        Play(GetVoiceAudioSource(),
            GetAudioFromResources(
                GetSoundVoicePath() + nameVoiceSound),
            delay,
            false);
    }

    public void PlayVoice(AudioClip voiceSound) {
        Play(GetVoiceAudioSource(),voiceSound,false);
    }

    public void PlayBackSound(string nameBackSound) {
        Play(GetBackMusicAudioSource(),
            GetAudioFromResources(
                GetBackMusicPath() + nameBackSound),
            true);
    }

    public void PlayBackSound(string nameBackSound,float delay) {
        Play(GetBackMusicAudioSource(),
            GetAudioFromResources(
                GetBackMusicPath() + nameBackSound),
            delay,
            true);
    }

    public void PlayBackSound(string nameBackSound,bool loop) {
        Play(GetBackMusicAudioSource(),
            GetAudioFromResources(
                GetBackMusicPath() + nameBackSound),
            loop);
    }

    public void PlayBackSound(AudioClip voiceSound) {
        Play(GetBackMusicAudioSource(),voiceSound,true);
    }

    public void PlayOtherSound(string nameSound) {
        Play(GetOtherSoundsAudioSource(),
            GetAudioFromResources(
                GetOtherSoundPath() + nameSound),
            false);

    }

    public void PlayOtherSound(string nameSound,bool loop) {
        Play(GetOtherSoundsAudioSource(),
            GetAudioFromResources(
                GetOtherSoundPath() + nameSound),
            loop);

    }

    public AudioClip GetVoiceByName(string nameClip) {
        return GetAudioFromResources(GetSoundVoicePath() + nameClip);
    }

    public AudioClip GetBackSoundByName(string nameClip) {
        return GetAudioFromResources(GetBackMusicPath() + nameClip);
    }

    public string GetSoundVoicePath() {
        string sceneName = SceneManager.GetActiveScene().name+"/";
        if (Application.systemLanguage == SystemLanguage.Russian) {
            return "Sounds/Ru/" + sceneName;
        }
        else {
            return "Sounds/Ru/" + sceneName;
        }
    }

    public string GetBackMusicPath() {
        string sceneName = SceneManager.GetActiveScene().name+"/";
        return "Sounds/BackSounds/" + sceneName;
    }

    public string GetOtherSoundPath() {
        return "Sounds/BackSounds/" + "OtherSounds/";
    }
}
