﻿using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {
    [SerializeField] private ObjectsTypes.Food tupe;
    private bool scale;
    private Vector3 targetScale;
    private float speedScale = 10f;
    private string[] namesVoice = {"22","22_1","22_2"};

    private void Start() {
        this.ScaleTo(new Vector3(1,1,1));
        this.gameObject.GetComponent<TouchHandler>().PointerDownEvent += this.OnPointerDownListener;
        this.gameObject.GetComponent<TouchHandler>().PointerUpEvent += this.OnPointerUpListener;
    }

    private void Update() {
        if (this.scale) {
            if (this.transform.localScale != this.targetScale) {
                this.transform.localScale = Vector3.MoveTowards(
                    this.transform.localScale,
                    this.targetScale,
                    this.speedScale*Time.deltaTime
                    );
            }
            else {
                this.scale = false;
                if (this.transform.localScale==Vector3.zero) {
                    Destroy(this.gameObject);
                }
            }
        }
    }

    public void ScaleTo(Vector3 target,float speed = 10f) {
        this.targetScale = target;
        this.scale = true;
         this.speedScale = speed;
    }

    public ObjectsTypes.Food GetFoodType() {
        return this.tupe;
    }

    private void OnPointerDownListener(GameObject obj) {
        this.transform.localScale = this.transform.localScale*1.2f;
    }

    private void OnPointerUpListener(GameObject obj) {
        if (this.GetFoodType() == ObjectsTypes.Food.EATABLE) {
            KitchenManager.instance.StopAllCoroutines();
            GetComponent<Mover>().MoveTo(KitchenManager.instance.GetMouthPosition(), 550f);
            ScaleTo(Vector3.zero, 2.5f);
            KitchenManager.instance.GetBearAnimation().PlaySmileAnimaton();
            KitchenManager.instance.Wait(2f);
            KitchenManager.instance.ForcedRemovalOfFood();
            KitchenManager.instance.Eat();
        }
        else {
            KitchenManager.instance.GetBearAnimation().PlaySadAnimation();
            this.transform.localScale = this.transform.localScale / 1.2f;
            KitchenManager.instance.StopAllCoroutines();
            KitchenManager.instance.Wait(0.2f/*KitchenManager.instance.GetDestroyTime()*/);
            KitchenManager.instance.ForcedRemovalOfFood();
            SoundsManager.instance.PlayVoice(this.namesVoice[
                KitchenManager.instance.GetNameVoiceID(this.namesVoice.Length)]);
        }
    }

}
