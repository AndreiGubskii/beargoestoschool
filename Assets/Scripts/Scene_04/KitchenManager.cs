﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.SceneManagement;

public class KitchenManager : MonoBehaviour {
    public static KitchenManager instance;
    [SerializeField] private GameObject[] eatable;
    [SerializeField] private GameObject[] inedible;
    [SerializeField] private int maxCountEatable;
    [SerializeField] private int maxCountInedible;
    [SerializeField] private Vector3[] positions;
    [SerializeField] private Transform parent;

    [SerializeField] private float delayBeforeStart;
    [SerializeField] private float repeatTime;
    [SerializeField] private float destroyTime;
    [SerializeField] private Vector3 mouthPosition;
    [SerializeField] private UIBearAnimationController bearAnimation;
    [SerializeField] private int hunger;
    [SerializeField] private CounterController counter;

    private List<Vector3> possitionList;
    private List<GameObject> foodList;
    private int countEatable;
    private int countInedible;
    private bool destroyObject;
    private float startTime;
    private bool inScene;
    private bool wait;
    private bool end;

    private int nameVoiceID = 0;
    private int prevNameVoiceID = 0;

    private void Awake() {
        instance = this;
        
    }

    private void Start() {
        this.Restart();
    }

    private void Update() {
        if (this.counter.GetCount() >= this.hunger) {
            StartCoroutine(this.End(1f));
        }
        if (this.wait || this.end) return;
        if (this.destroyObject || ((this.GetDestroyTime() + this.startTime)<Time.time && this.inScene)) {
            this.DestroyFood();
        }

        if (this.destroyObject || ((this.startTime + (this.GetDestroyTime() + this.repeatTime)) < Time.time && !this.inScene)) {
            this.InstantiateFood();
            this.destroyObject = false;
        }


    }

    private void DestroyFood() {
        foreach (GameObject food in this.foodList) {
            if (food != null) {
                food.GetComponent<FoodController>().ScaleTo(Vector3.zero);
            }
        }
        this.inScene = false;
    }

    private void InstantiateFood() {
        this.possitionList = new List<Vector3>();
        this.foodList = new List<GameObject>();
        foreach (Vector3 position in positions) {
            this.possitionList.Add(position);
        }
        this.countEatable = 0;
        this.countInedible = 0;
        for (int i = 0; i <= possitionList.Count; i++) {
            if (this.countEatable < this.maxCountEatable) {
                this.foodList.Add(this.InstantiateEatable());
            }
            if (this.countInedible<this.maxCountInedible){
                this.foodList.Add(this.InstantiateInedible());
            }
        }
        this.startTime = Time.time;
        this.inScene = true;
    }

    private GameObject InstantiateEatable() {
        GameObject ob = Instantiate(this.eatable[Random.Range(0, this.eatable.Length)]);
        ob.transform.SetParent(this.parent);
        ob.transform.localPosition = this.GetPosition();
        ob.transform.localScale = new Vector3(0,0,0);
        this.countEatable++;
        return ob;
    }

    private GameObject InstantiateInedible() {
        GameObject ob = Instantiate(this.inedible[Random.Range(0, this.inedible.Length)]);
        ob.transform.SetParent(this.parent);
        ob.transform.localPosition = this.GetPosition();
        ob.transform.localScale = new Vector3(0, 0, 0);
        this.countInedible++;
        return ob;
    }

    private Vector3 GetPosition() {
        Vector3 pos = possitionList[Random.Range(0, this.possitionList.Count)];
        this.possitionList.Remove(pos);
        return pos;
    }

    public Vector3 GetMouthPosition() {
        return this.mouthPosition;
    }

    public UIBearAnimationController GetBearAnimation() {
        return this.bearAnimation;
    }

    public void ForcedRemovalOfFood() {
        this.destroyObject = true;
    }

    public void Wait(float waitTime) {
        StartCoroutine(WaitIEnumerator(waitTime));
    }

    private IEnumerator WaitIEnumerator(float waitTime) {
        this.wait = true;
        yield return new WaitForSeconds(waitTime);
        this.wait = false;
    }

    public float GetDestroyTime() {
        return this.destroyTime;
    }

    public void Eat() {
        SoundsManager.instance.StopAllCoroutines();
        StartCoroutine(this.EatIEnumerator(0.7f));
        SoundsManager.instance.PlayOtherSound("hrum");
    }

    private IEnumerator EatIEnumerator(float delay) {
        yield return new WaitForSeconds(delay);
        this.counter.Count();
    }

    private IEnumerator End(float delay) {
        AudioClip voice = SoundsManager.instance.GetVoiceByName("23");
        SoundsManager.instance.PlayVoice(voice);
        this.end = true;
        this.counter.ResetCounter();
        yield return new WaitForSeconds(delay);
        this.DestroyFood();
        this.GetBearAnimation().PlaySmileOnce();
        if (MainManager.instance != null) {
            MainManager.instance.ShowEndPanel(voice.length, 0f, SoundsManager.instance.GetVoiceByName("23_1"));
        }
    }

    public void Restart() {
        SoundsManager.instance.PlayBackSound("kitchen_sound");
        AudioClip voice = SoundsManager.instance.GetVoiceByName("19");
        SoundsManager.instance.PlayVoice(voice);
        SoundsManager.instance.PlayVoice("20",voice.length);

        this.end = false;
        this.GetBearAnimation().Reset();
        this.startTime = Time.time + voice.length - 1f;
        this.counter.SetMaxCount(this.hunger);
        this.counter.SetText(0, this.counter.GetMaxCount());
        this.nameVoiceID = 0;
        this.prevNameVoiceID = 0;
    }

    public int GetNameVoiceID(int length) {
        while (prevNameVoiceID == nameVoiceID) {
            prevNameVoiceID = Random.Range(0, length);
        }
        this.nameVoiceID = prevNameVoiceID;

        return this.nameVoiceID;
    }
}
