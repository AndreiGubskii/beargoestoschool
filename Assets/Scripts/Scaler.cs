﻿using UnityEngine;
using System.Collections;

public class Scaler : MonoBehaviour {
    private bool scale;
    private Vector3 targetScale;
    private float speedScale;

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    private void Update() {
        if (this.scale) {
            if (this.transform.localScale != this.targetScale) {
                this.transform.localScale = Vector3.MoveTowards(
                    this.transform.localScale,
                    this.targetScale,
                    this.speedScale*Time.deltaTime
                    );
            }
            else {
                this.scale = false;
            }
        }
    }

    public void ScaleTo(Vector3 target,float speed = 10f) {
        this.targetScale = target;
        this.speedScale = speed;
        this.scale = true;
    }

    public void ScaleTo(float delay,Vector3 target,float speed = 10f) {
        StartCoroutine(this.ScaleToIEnumerator(delay, target, speed));
    }

    private IEnumerator ScaleToIEnumerator(float delay, Vector3 target, float speed = 10f) {
        yield return new WaitForSeconds(delay);
        ScaleTo(target,speed);
    }
}
