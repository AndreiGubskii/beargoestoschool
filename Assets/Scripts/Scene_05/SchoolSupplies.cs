﻿using UnityEngine;
using System.Collections;
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
using UnityEngine.iOS;
#endif
public class SchoolSupplies : MonoBehaviour {
    public delegate void SchoolSuppliesEvents(ObjectsTypes.SchoolSuppliesName name);

    public event SchoolSuppliesEvents OnClickForShool;
    public event SchoolSuppliesEvents OnClickNotForSchool;

    [System.Serializable]
    public class ClothingTransform {
        public Vector3 position;
        public Vector3 rotation;
        public Vector3 scale;
    }
    private bool scale;
    private Vector3 targetScale;
    private float speedScale = 10f;
    private float scaleOnDown = 1.3f;
    [SerializeField] private ObjectsTypes.SchoolSuppliesType schoolSuppliesType;
    [SerializeField] private ObjectsTypes.SchoolSuppliesName nameSchoolSupplies;
    [SerializeField] private ClothingTransform[] startTransform;
    [SerializeField] private ClothingTransform[] iPadStartTransform;
    [SerializeField] private ClothingTransform[] iPhoneXStartTransformArray;
    [SerializeField] private bool additionalSettings;
    [SerializeField] private Vector3 backpackPosition = new Vector3(730f, 420f, 0f);
    [SerializeField] private Vector3 iPadBackpackPosition = new Vector3(550f, 440f, 0f);
    private TouchHandler touchHandler;
    private ClothingTransform startTransformValues;
    
    private void OnEnable() {
        this.touchHandler = GetComponent<TouchHandler>();
        int i = Random.Range(0, GetStartTransformArray().Length);
        this.startTransformValues = GetStartTransformArray()[i];

        this.transform.localPosition = this.startTransformValues.position;
        this.transform.localRotation = Quaternion.Euler(this.startTransformValues.rotation);
        this.transform.localScale = this.startTransformValues.scale;

        this.touchHandler.PointerDownEvent += this.OnPointerDownListener;
        this.touchHandler.PointerUpEvent += this.OnPointerUpListener;
        this.scale = false;
    }

    private void OnDisable() {
        this.touchHandler.PointerDownEvent -= this.OnPointerDownListener;
        this.touchHandler.PointerUpEvent -= this.OnPointerUpListener;

    }

    private void Update() {
        if (this.scale) {
            if (this.transform.localScale != this.targetScale) {
                this.transform.localScale = Vector3.MoveTowards(
                    this.transform.localScale,
                    this.targetScale,
                    this.speedScale*Time.deltaTime
                    );
            }
            else {
                this.scale = false;
                if (this.transform.localScale==Vector3.zero) {
                    this.gameObject.SetActive(false);
                }
            }
        }

    }

    public void ScaleTo(Vector3 target,float speed = 10f) {
        this.targetScale = target;
        this.scale = true;
        this.speedScale = speed;
    }

    public ObjectsTypes.SchoolSuppliesType GetSchoolSuppliesType() {
        return this.schoolSuppliesType;
    }

    private void OnPointerDownListener(GameObject go) {
        this.transform.localScale *= this.scaleOnDown;
    }

    private void OnPointerUpListener(GameObject go) {
        this.transform.localScale /= this.scaleOnDown;
        if (this.GetSchoolSuppliesType() == ObjectsTypes.SchoolSuppliesType.FORSHOOL) {
            Vector3 target;
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
            if (Device.generation.ToString().IndexOf("iPad") > -1) {
                target = iPadBackpackPosition;
            }else{
                target = backpackPosition;
            }
#endif
#if (UNITY_ANDROID || UNITY_WSA) && !UNITY_IOS
            target = backpackPosition;
#endif
            GetComponent<Mover>().MoveTo(target,5300f);
            this.ScaleTo(Vector3.zero,1f);
            if (this.OnClickForShool != null) {
                this.OnClickForShool(this.GetNameSchoolSupplies());
            }
        } else if (this.GetSchoolSuppliesType() == ObjectsTypes.SchoolSuppliesType.NOTFORSHOOL) {
            if (this.OnClickNotForSchool != null) {
                this.OnClickNotForSchool(this.GetNameSchoolSupplies());
            }
        }
    }

    public ObjectsTypes.SchoolSuppliesName GetNameSchoolSupplies() {
        return this.nameSchoolSupplies;
    }

    private ClothingTransform[] GetStartTransformArray() {
        if (this.additionalSettings) {
#if (!UNITY_ANDROID || !UNITY_WSA) && UNITY_IOS
                if (Device.generation.ToString().IndexOf("iPad") > -1) {
                    return this.iPadStartTransform;
                }
                else if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXR ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXS ||
                UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneXSMax) {
                    return this.iPhoneXStartTransformArray;
                }
                else{
                    return this.startTransform;
                }
#endif
#if (UNITY_ANDROID || UNITY_WSA) && !UNITY_IOS
            return this.startTransform;
#endif
        }
        else {
                return this.startTransform;
        }
    }
}
