﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SearchSchoolSupplies : MonoBehaviour {
    public static SearchSchoolSupplies instance;
    [SerializeField] private UIBearAnimationController bearAnimation;
    [SerializeField] private SchoolSupplies[] schoolSupplies;
    [SerializeField] private int quantityItemsInBackpack;
    [SerializeField] private CounterController counter;
    private int counterItemsInBackpack = 0;
    private bool end;

    private void Awake() {
        instance = this;
        this.schoolSupplies = FindObjectsOfType<SchoolSupplies>();
    }

    private void Start() {
        foreach (var supply in schoolSupplies) {
            supply.OnClickForShool += this.ClickForSchoolListener;
            supply.OnClickNotForSchool += this.ClickForNotSchoolListener;
        }
        this.counter.SetMaxCount(this.quantityItemsInBackpack);
        this.counter.SetText(this.counter.GetCount(),this.counter.GetMaxCount());
        SoundsManager.instance.PlayVoice("24");
        SoundsManager.instance.PlayBackSound("search_sound");
    }

    private void Update() {
        if (!this.end && (this.counter.GetCount() == this.quantityItemsInBackpack)) {
            this.End();
        }
    }

    public UIBearAnimationController GetBearAnimation() {
        return this.bearAnimation;
    }

    private void ClickForSchoolListener(ObjectsTypes.SchoolSuppliesName nameObj) {
        if(this.end) return;
        this.bearAnimation.PlaySmileAnimaton();
        switch (nameObj) {
            case ObjectsTypes.SchoolSuppliesName.BOOKS:
                SoundsManager.instance.PlayVoice("26");
                break;
            case ObjectsTypes.SchoolSuppliesName.PEN:
                SoundsManager.instance.PlayVoice("27");
                break;
            case ObjectsTypes.SchoolSuppliesName.PENCIL:
                SoundsManager.instance.PlayVoice("28");
                break;
            case ObjectsTypes.SchoolSuppliesName.NOTEBOOK:
                SoundsManager.instance.PlayVoice("25");
                break;
            case ObjectsTypes.SchoolSuppliesName.RULER:
                SoundsManager.instance.PlayVoice("25_1");
                break;
        }
    }

    private void ClickForNotSchoolListener(ObjectsTypes.SchoolSuppliesName nameObj) {
        if (this.end) return;
        this.bearAnimation.PlaySadAnimation();
        SoundsManager.instance.PlayVoice("29");
        switch (nameObj) {
            case ObjectsTypes.SchoolSuppliesName.AIRCRAFT:
                SoundsManager.instance.PlayOtherSound("aircraft");
                break;
            case ObjectsTypes.SchoolSuppliesName.CAR:
                SoundsManager.instance.PlayOtherSound("car");
                break;
            case ObjectsTypes.SchoolSuppliesName.DUCK:
                SoundsManager.instance.PlayOtherSound("duck");
                break;
            default:
                SoundsManager.instance.PlayOtherSound("objectclick");
                break;
        }
    }

    private void End() {
        this.end = true;
        this.GetBearAnimation().PlaySmileOnce();
        AudioClip voice = SoundsManager.instance.GetVoiceByName("30");
        SoundsManager.instance.PlayVoice(voice);
        if (MainManager.instance != null) {
            MainManager.instance.ShowAds();
            MainManager.instance.ShowEndPanel(voice.length, 0f, SoundsManager.instance.GetVoiceByName("30_1"));
        }
    }

    public void Restart() {
        SoundsManager.instance.PlayBackSound("search_sound");
        this.counter.ResetCounter();
        this.counter.SetText(this.counter.GetCount(), this.counter.GetMaxCount());
        this.GetBearAnimation().Reset();
        foreach (SchoolSupplies schoolSuppliese in schoolSupplies) {
            schoolSuppliese.gameObject.SetActive(false);
            schoolSuppliese.gameObject.SetActive(true);
        }
        this.end = false;
        SoundsManager.instance.PlayVoice("24");
    }
}
