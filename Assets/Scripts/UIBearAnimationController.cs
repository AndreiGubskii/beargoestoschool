﻿using UnityEngine;
using System.Collections;

public class UIBearAnimationController : MonoBehaviour {

    private Animator _animator;
    private int hashEars = Animator.StringToHash("ears");
    private int hashRolleyes = Animator.StringToHash("rolleyes");
    private int hashSmile = Animator.StringToHash("smile");
    private int hashSmileOnce = Animator.StringToHash("smileOnce");
    private int hashSad = Animator.StringToHash("sad");
    private int hashReset = Animator.StringToHash("reset");
    private float timer = 2f;
    private bool smileOnce;
    private float time;
    void Start () {
	    this._animator = GetComponent<Animator>();
        this.time = Time.time;
    }

    private void OnEnable() {
        this.smileOnce = false;
    }

    private void Update() {
        if (this.smileOnce) return;
        if ((this.time + this.timer) < Time.time) {
            this.PlayRandomAnimation();
            this.time = Time.time;
            this.timer = Random.Range(2f, 5f);
        }
    }

    public void PlayEarsAnimation() {
        this._animator.SetTrigger(this.hashEars);
    }

    public void PlayRolleyesAnimation() {
        this._animator.SetTrigger(hashRolleyes);
    }

    public void PlaySmileAnimaton() {
        this._animator.SetTrigger(hashSmile);
    }

    public void PlaySadAnimation() {
        this._animator.SetTrigger(hashSad);
    }

    public void PlaySmileOnce() {
        this._animator.SetTrigger(hashSmileOnce);
        this.smileOnce = true;
    }

    public void Reset() {
        if (this._animator != null) {
            this._animator.SetTrigger(hashReset);
        }
        this.smileOnce = false;
    }

    private void PlayRandomAnimation() {
        int i = Random.Range(0, 11);
        if ((i%2) > 0) {
            this.PlayEarsAnimation();
        }
        else {
            this.PlayRolleyesAnimation();
        }
    }
}
